package main

import "fmt"

type taskList struct{
  tasks []*task
}

func (t *taskList) agregarALista( tl * task) {
  t.tasks = append(t.tasks, tl)
}

type task struct{
  nombre string
  descripcion string
  completado bool

}

func (t *task) marcarCompleta() {
  t.completado = true
}

func (t *task) actualizarDescripcion(descripcion string) {
  t.descripcion = descripcion
}

func (t *task) actualizarNombre(nombre string) {
  t.nombre = nombre
}

func (t *taskList) eliminarDeList(index int) {
  t.tasks = append(t.tasks[:index], t.tasks[index + 1:]...)
}

func (t *taskList)  imprimirList() {
  for _, tarea := range t.tasks {
    fmt.Println("Nombre", tarea.nombre)
    fmt.Println("descripcion", tarea.descripcion)

  }
}

func (t *taskList)  imprimirListCompletados() {
  for _, tarea := range t.tasks {
    if tarea.completado {
      fmt.Println("Nombre", tarea.nombre)
      fmt.Println("descripcion", tarea.descripcion)
    }
  }
}


func main(){

  t := &task{
    nombre: "Completar mi curso de Go",
    descripcion: "Completar mi curso de Go de Platzi en esta semana",
  }

  t2 := &task{
      nombre: "Completar mi curso de Python",
      descripcion: "Completar mi curso de Python de Platzi en esta semana",
  }

  t3 := &task{
      nombre: "Completar mi curso de Nodejs",
      descripcion: "Completar mi curso de Nodejs de Platzi en esta semana",
  }

  list := &taskList { 
     tasks: []*task{
      t, t2,
    },
  }

  // Agregar tarea a la lista
  fmt.Println(list.tasks[0])
  list.agregarALista(t3)
  fmt.Println(len(list.tasks))
  fmt.Println("Tarea agregado a la lista")


  // Eliminar tarea de lista
  list.eliminarDeList(1)
  fmt.Println(len(list.tasks))
  fmt.Println("Elemento eliminado")


  // Recorrer los objetos

  for i := 0; i < len(list.tasks); i++ {
    fmt.Println("Index", i, "nombre", list.tasks[i].nombre)
  }


  // Range
  for index, tarea := range list.tasks{
    fmt.Println("\n")
    fmt.Println("Index", index, "nombre", tarea.nombre)
  }


  fmt.Println("break")
  for i := 0; i < 10; i++ {
    if i == 5 {
      // braeak
      continue
    }
    fmt.Println(i)
  }
  list.imprimirList()

  // completado
  list.tasks[0].marcarCompleta()
  fmt.Println("Tareas Completadas")
  list.imprimirListCompletados()

  // fmt.Printf("%+v\n", t)
  // t.marcarCompleta()
  // t.actualizarNombre("Finalizar mi curso de Go")
  // t.actualizarDescripcion("Completar mi curso de Go cuanto antes")
  // fmt.Printf("%+v\n", t)

  mapaTareas := make(map[string]*taskList)
  mapaTareas["Nestor"] = list

  t4 := &task{
      nombre: "Completar mi curso de Java",
      descripcion: "Completar mi curso de Java de Platzi en esta semana",
  }

  t5 := &task{
      nombre: "Completar mi curso de C#",
      descripcion: "Completar mi curso de C# de Platzi en esta semana",
  }

  list2 := &taskList{
    tasks: []*task{
      t4, t5,
    },
  }

  mapaTareas["Ricardo"] = list2
  fmt.Println("Tareas de Nestor")
  mapaTareas["Nestor"].imprimirList()

  fmt.Println("Tareas de Ricardo")
  mapaTareas["Ricardo"].imprimirList()
}

