## Compilar Go

```sh
# go build <name-file>
go build main.go
```

## Ejecutar y compilar

```sh
# go run <file-name>
go run main.go
```

## crear Modulo en go

```sh
# go mod <name-modulo>
go mod
```

