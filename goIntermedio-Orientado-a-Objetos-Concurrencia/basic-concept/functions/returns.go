package main

import (
	"fmt"
)

// function retorna el resultado de la suma de los argumentos
func sum(value ...int) int {
	total := 0
	for _, num := range value {
		total += num
	}
	return total
}

// function retorna un string -> de nombres
func printNames(names ...string) {
	for _, name := range names {
		fmt.Println(name)
	}
}

// Function que retorna enteros
func getValues(x int) (double int, triple int, quad int) {
	// return 2 * x, 3 * x, 4 * x
	double = 2 * x
	triple = 3 * x
	quad = 4 * x
	return
}

func main()  {
	fmt.Println(sum(5))
	fmt.Println(sum(5, 5))
	fmt.Println(sum(5, 5, 3, 4))
	printNames("John", "Jane", "Joe", "Alice")
	fmt.Println(getValues(2))
}