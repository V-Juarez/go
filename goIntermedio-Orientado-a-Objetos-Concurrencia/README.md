<h1>Go Intermedio: Programación Orientada a Objetos y Concurrencia</h1>

<h3>Néstor Escoto</h3>

<h1>Table of Content</h1>

- [1. Introducción](#1-introducción)
	- [Características esenciales de Go](#características-esenciales-de-go)
	- [Qué aprenderás y qué necesitas saber](#qué-aprenderás-y-qué-necesitas-saber)
	- [Repaso general: variables, condicionales, slices y map](#repaso-general-variables-condicionales-slices-y-map)
	- [Repaso general: GoRoutines y apuntadores](#repaso-general-goroutines-y-apuntadores)
- [2. Programación orientada a objetos](#2-programación-orientada-a-objetos)
	- [¿Es Go orientado a objetos?](#es-go-orientado-a-objetos)
	- [Structs vs. clases](#structs-vs-clases)
	- [Métodos y funciones](#métodos-y-funciones)
	- [Constructores](#constructores)
	- [Herencia](#herencia)
	- [Interfaces](#interfaces)
	- [Aplicando interfaces con Abstract Factory](#aplicando-interfaces-con-abstract-factory)
	- [Implementación final de Abstract Factory](#implementación-final-de-abstract-factory)
	- [Funciones anónimas](#funciones-anónimas)
	- [Funciones variadicas y retornos con nombre](#funciones-variadicas-y-retornos-con-nombre)
- [3. Go Modules](#3-go-modules)
	- [Cómo utilizar los Go modules](#cómo-utilizar-los-go-modules)
	- [Creando nuestro módulo](#creando-nuestro-módulo)
- [4. Testing](#4-testing)
	- [Testing](#testing)
	- [Code coverage](#code-coverage)
	- [Profiling](#profiling)
	- [Testing usando Mocks](#testing-usando-mocks)
	- [Implementando Mocks](#implementando-mocks)
- [5. Concurrencia](#5-concurrencia)
	- [Unbuffered channels y buffered channels](#unbuffered-channels-y-buffered-channels)
	- [Waitgroup](#waitgroup)
	- [Buffered channels como semáforos](#buffered-channels-como-semáforos)
	- [Definiendo channels de lectura y escritura](#definiendo-channels-de-lectura-y-escritura)
	- [Worker pools](#worker-pools)
		- [Explanation](#explanation)
	- [Multiplexación con Select y Case](#multiplexación-con-select-y-case)
- [6. Proyecto: servidor con worker pools](#6-proyecto-servidor-con-worker-pools)
	- [Definiendo workers, jobs y dispatchers](#definiendo-workers-jobs-y-dispatchers)
	- [Creando web server para procesar jobs](#creando-web-server-para-procesar-jobs)
- [7. Conclusión](#7-conclusión)
	- [Continúa con el Curso de Go Avanza](#continúa-con-el-curso-de-go-avanza)

# 1. Introducción

## Características esenciales de Go

**Golang**

- Lenguaje compilado
- Potente librería estándar
- Manejo de concurrencia a través de GoRoutines y Channels.
- Diseñado por Google, Ken Thompson (UNIX) parte del equipo de diseño
- Muy popular en aplicaciones CLI y Backend
- Docker, Kubernetes y Terraform están escritos en Go.
- Muy utilizado para escribir malware.
- Según StackOverflow, es el tercer lenguaje mejor pagado a nivel global.
- Según StackOverflow, es la quinta tecnología mas amada por los desarrolladores y la tercera mas deseada para trabajar.

![img](https://712431.smushcdn.com/1410584/wp-content/uploads/2018/07/Benefits-of-using-golang.png)

- lenguaje compilado
- buena librería estándar
- concurrencia simple pero potente
- usado en backend, tooling, devops, incluso en malwares

[![img](https://www.google.com/s2/favicons?domain=https://www.cncf.io/projects//wp-content/themes/lf-theme/images/apple-touch-icon.png)Graduated and incubating projects | Cloud Native Computing Foundation](https://www.cncf.io/projects/)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.sstatic.net/Sites/stackoverflow/Img/favicon.ico?v=ec617d715196)Stack Overflow Developer Survey 2020](https://insights.stackoverflow.com/survey/2020#top-paying-technologies)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.sstatic.net/Sites/stackoverflow/Img/favicon.ico?v=ec617d715196)Stack Overflow Developer Survey 2020](https://insights.stackoverflow.com/survey/2020#technology-most-loved-dreaded-and-wanted-languages-wanted)

## Qué aprenderás y qué necesitas saber

**Conocimientos Requeridos**

- Declaración de variables.
- Condicionales.
- Sintaxis básica.
- Declaración de GoRoutines y Channels.
- Slices y maps.
- Apuntadores.

**¿Qué terminarás sabiendo?**

- ¿Es Go orientado a objetos?
- Cómo aplicar los conceptos de POO en Golang.
- Crear Test Unitarios en Go para aplicar TDD.
- Calcular el Code Coverage de mi proyecto.
- Análisis del Profiling en tus programas.
- Cómo multiplexar mensajes de canales.
- Técnicas para la creación de Worker Pools concurrentes.
- Crear Test Unitarios en Go para aplicar TDD.
- Crear un Job Queue concurrente.

## Repaso general: variables, condicionales, slices y map

```go
Codigo de la clase:

package main

import (
	"fmt"
	"strconv"
)

func main() {

	var x int
	x = 8
	y := 7
	fmt.Println(x)
	fmt.Println(y)

	// Capturando valor y error
	myValue, err := strconv.ParseInt("NaN", 0, 64)

	// Validando errores.
	if err != nil {
		fmt.Println("%v\n", err)
	} else {
		fmt.Println(myValue)
	}

	// Mapa clave valor.
	m := make(map[string]int)
	m["key"] = 6
	fmt.Println(m["key"])

	// Slice de enteros.
	s := []int{1, 2, 3}
	for index, value := range s {
		fmt.Println(index)
		fmt.Println(value)
	}
	s = append(s, 16)
	for index, value := range s {
		fmt.Println(index)
		fmt.Println(value)
	}
}
```



## Repaso general: GoRoutines y apuntadores

Codigo acumulado de las clases:

```go
package main

import (
	"fmt"
	"strconv"
	"time"
)

func main() {
	var x int
	x = 8
	y := 7
	fmt.Println(x)
	fmt.Println(y)

	// Capturando valor y error
	myValue, err := strconv.ParseInt("NaN", 0, 64)

	// Validando errores.
	if err != nil {
		fmt.Println("%v\n", err)
	} else {
		fmt.Println(myValue)
	}

	// Mapa clave valor.
	m := make(map[string]int)
	m["key"] = 6
	fmt.Println(m["key"])

	// Slice de enteros.
	s := []int{1, 2, 3}
	for index, value := range s {
		fmt.Println(index)
		fmt.Println(value)
	}
	s = append(s, 16)
	for index, value := range s {
		fmt.Println(index)
		fmt.Println(value)
	}

	//c := make(chan int)
	//go doSomething(c)
	//<-c

	g := 25
	fmt.Println(g) // imprime el valor entero 25
	h := &g
	fmt.Println(h) // imprimer la direccion de memoria.
	i := *h
	fmt.Println(i) // Imprime el valor por de g
}

func doSomething(c chan int) {
	time.Sleep(3 * time.Second)
	fmt.Println("done")
	c <- 1
}
```

Gorutines and chanels

```go
package main

import "time"

func main() {
	c := make(chan int)
	go doSomething(c)
	go doOtherThing()
	go doSomething(c)
	<- c
	print("Lo imprimirá?")

}

func doSomething(c chan int)  {
	time.Sleep(3 * time.Second)
	println("Do something")
	c <- 1
}

func doOtherThing(){
	println("quien se imprimirá primero?")
}
```

[Go con ejemplos](https://gobyexample.com/goroutines)

# 2. Programación orientada a objetos

## ¿Es Go orientado a objetos?

Es todo acerca de tipo
Es go un lenguaje orientado a objetos? Go tiene aspectos de OOP. Pero todo es acerca de TIPOS. Creamos TIPOS en Go; TIPOS definidos por el usuario. Entonces podemos tener VALORES de ese tipo.
Podemos asignar VALORES de un TIPO definido por el usuario a VARIABLES.
**Go es Orientado a Objetos**

1. **Encapsulación**
   a. Estado(“campos”)
   b. Comportamiento (“métodos”)
   c. Exportado & No exportado; visible & no visible
   **Reusabilidad**
   a. herencia (“tipos embebidos”)
   **Polimorfismo**
   a. interfaces
   **Overriding**
   a. "promoción"
   **OOP Tradicional**
2. Clases
   a. Estructura de dato describiendo un tipo de objeto
   b. Puedes crear “instancias”/ “objetos” de la clase / prototipo
   c. Las clases almacenan ambos:
   i. estado / datos / campos
   ii. comportamiento / métodos
   d. público / privado
3. **Herencia**

- **En Go:**

1. No creas clases, creas un **TIPO**
2. No creas instancias, creas un **VALOR** de un **TIPO**
   **Tipos definidos por el usuario**

- Podemos declarar un nuevo tipo
- foo
- El tipo subyacente de foo es int
- Conversión a int
- int(miEdad)
- Convirtiendo tipo foo a tipo int
- ES UNA MALA PRÁCTICA HACER ALIAS DE TIPOS
- Excepción:
- Si necesitas asignarle métodos a un tipo
- Ver el paquete de tiempo para un ejemplo
  type Duration int64
  Duration tiene métodos asignados
- Tipos Nombrados vs Tipos Anónimos

Tipos anónimos son indeterminados. Aún no han sido declarados como un tipo. El compilador tiene flexibilidad con tipos anónimos. Puedes asignar un tipo anónimo a una variable declarada de cierto tipo. Si la asignación puede ocurrir, el compilador hará el trabajo de determinar el tipo; el compilador hará una conversión implícita. No puedes asignar un tipo nombrado a un tipo de diferente nombre.
Alineamiento arquitectónico y más.

Convención: organiza tus campos lógicamente. Legibilidad y claridad ganan en rendimiento como punto crítico. Go será de buen rendimiento. Ve primero por legibilidad. Sin embargo, si estás en una situación donde necesitas darle prioridad al rendimiento: agrega los campos del más grande al de menor tamaño, por ejemplo: int 64, int32, float32, bool.

***¿Es Go Orientado a objetos?***

- POO se ha convertido en uno de los paradigmas de programación predominante en la industria.
- POO puede llegar a ser muy riguroso, pero a cambio permite una alta reutiliuzación de código y la aplicación de un sinnúmero de patrones de diseño.
- Go puede alcanzar la aplicación de los conceptos de POO, pero de una forma diferente a lenguajes como Java y Python.

## Structs vs. clases

- El objetivo de una clase es definir una serie de propiedades y métodos que un objeto puede usar para
  alcanzar diferentes objetivos.
- Go utiliza Structs para generar “nuevos tipos” de datos que se pueden utilizar para cumplir tareas en
  específico.

`main`:

```go
package main

import "fmt"

type Employee struct {
	id   int
	name string
}

func main() {
	e := Employee{}
	fmt.Println("%v", e)

	e.id = 1
	e.name = "Name"
	fmt.Println("%v", e)
}
```

## Métodos y funciones

Super importante este apartado para entender las diferencias entre los value y pointers receivers.
https://tour.golang.org/methods/4

Básicamente si queres actualizar los valores de tu struct “instanciado” usa pointer, en otras ocasiones, como por ejemplo solo usar los valores del struct, se puede usar value reciver.

struct Employee en Go 😄

```go
package main

import "fmt"

type Employee struct {
	id   int
	name string
}

func (e *Employee) SetId(id int) {
	e.id = id
}

func (e *Employee) SetName(name string) {
	e.name = name
}

func (e *Employee) GetId() int {
	return e.id
}

func (e *Employee) GetName() string {
	return e.name
}

func main() {
	e := Employee{}
	e.id = 1
	e.name = "Ana"
	e.SetId(5)
	e.SetName("Luis")
	fmt.Println(e.GetId())
	fmt.Println(e.GetName())
}
```

codigo con algunos comentarios:

```go
package main

import "fmt"

// Clase o struct empleado
type Employee struct {
	id   int
	name string
}

// funcion o metodo SetId
func (e *Employee) SetId(id int) {
	e.id = id
}

// funcion o metodo SetName
func (e *Employee) SetName(name string) {
	e.name = name
}

// funcion o metodo para obtener el id
func (e *Employee) GetId() int {
	return e.id
}

// funcion o metodo para obtener el nombre
func (e *Employee) GetName() string {
	return e.name
}

func main() {
	e := Employee{}

	// asignamos valores directamente.
	e.id = 1
	e.name = "Name"
	fmt.Println("%v", e)

	// asignamos valores mediante metodos.
	e.SetId(5)
	e.SetName("Carlos")
	fmt.Println("%v", e)

	// imprimimos llamando a los metodos.
	fmt.Println(e.GetId())
	fmt.Println(e.GetName())

}
```

## Constructores

Todas las formas de instanciar un objeto en Go:

1. Crear una struct con valores `zero` por defecto.

```go
e  := Employee{}
```

1. Asignarle valores a las propiedades.

```go
e2 := Employee{ id: 1, name: "Name", vacation: true }
```

1. Usando la palabra reservada `new`.

> Regresa una referencia a la instancia creada, para acceder al valor usamos `*e3`.

```go
e3 := new(Employee)
```

⭐ 4. Crear una función que actúe como método constructor.

> La función regresa con `&` para que regrese la referencia y no una copia, de la `struct`.

```go
func NewEmployee(id int, name string, vacation bool) *Employee {
	return &Employee{
		id:       id,
		name:     name,
		vacation: vacation,
	}
}
...
e4 := NewEmployee(1, "Name 2", true)
```

codigo con comentarios:

```go
package main

import "fmt"

// Clase o struct empleado
type Employee struct {
	id       int
	name     string
	vacation bool
}

// funcion/metodo constructor.
func NewEmployee(id int, name string, vacation bool) *Employee {
	return &Employee{
		id:       id,
		name:     name,
		vacation: vacation,
	}
}

func main() {
	// forma numero 1.
	e := Employee{}
	fmt.Println("%v", e)

	// forma numero 2.
	e2 := Employee{
		id:   1,
		name: "Jose",
	}
	fmt.Println("%v", e2)

	// forma 3.
	e3 := new(Employee)    // new devuelve un apuntador
	fmt.Println("%v", *e3) // con * referenciamos al valor.

	// forma 4.
	e4 := NewEmployee(4, "Laura", false)
	fmt.Println(*e4)

}
```

- Los constructores permiten la instanciación de una clase a un objeto, asimismo permitedefinir propiedades predefinidas.
- En Go podemos utilizar funciones que puedan crear structs con propiedades que nosotrospasamos como parámetros.

```go
package main

import "fmt"

type Employee struct {
	id       int
	name     string
	vacation bool
}

//Emulador de constructor
func NewEmployee(id int, name string, vacation bool) *Employee {
	return &Employee{
		id:       id,
		name:     name,
		vacation: vacation,
	}
}

func main() {
	// Forma 1
	e := Employee{}
	fmt.Printf("%v\n", e)
	// Forma 2
	e2 := Employee{
		id:       1,
		name:     "Benjamin",
		vacation: true,
	}
	fmt.Printf("%v\n", e2)
	// Forma 3
	e3 := new(Employee)
	fmt.Printf("%v\n", *e3)
	e3.id = 1
	e3.name = "Mateo"
	fmt.Printf("%v\n", *e3)
	// Forma 4
	e4 := NewEmployee(1, "Name", false)
	fmt.Printf("%v\n", *e4)
}
```

## Herencia

Go no permite la herencia, go utiliza la composicion.
la composicion, a diferencia de la herencia, no es una clase hija de… sino que contiene los metodos de las clases indicadas.

codigo:

```go
package main

import "fmt"

type Person struct {
	name string
	age  int
}

type Employee struct {
	id int
}

type FullTimeEployee struct {
	Person
	Employee
}

func main() {
	ftEmployee := FullTimeEployee{}
	ftEmployee.id = 1
	ftEmployee.name = "Maria"
	ftEmployee.age = 27
	fmt.Printf("%v", ftEmployee)
}
```

La siguiente función es un método para que `FullTimeEmployee` imprima sus atributos de forma más ✨ bonita ✨:

```go
func (employee FullTimeEmployee) String() string {
	return fmt.Sprintf("\nid: %d, name: %s, age: %d ", employee.id, employee.name, employee.age)
}
```

## Interfaces

 implementaciones de getMessage para `FullTimeEmployee` y `TemporaryEmployee` 😃:

```go
func (ftEmployee FullTimeEmployee) getMessage() string {
	return fmt.Sprintf("\nFull Time Employee\nid: %d name: %s age: %d endDate: %s",
		ftEmployee.id,
		ftEmployee.name,
		ftEmployee.age,
		ftEmployee.endDate)
}
func (tEmployee TemporaryEmployee) getMessage() string {
	return fmt.Sprintf("\nTemporary Employee\nid: %d name: %s age: %d taxRate: %d",
		tEmployee.id,
		tEmployee.name,
		tEmployee.age,
		tEmployee.taxRate)
}
```

- Diferentes lenguajes de programación utilizan sintaxis explicita para decir que una clase implementa un interfaz
- Go lo hace de manera implícita lo que permite la re utilización de código y polimorfismo

```go
package main

import "fmt"

type Person struct {
	name string
	age  int
}

type Employee struct {
	id int
}

// De esta manera aplicamos la composicion sobre la herencia
type FullTimeEmployee struct {
	Person
	Employee
	endDate string
}

type TemporatyEmployee struct {
	Person
	Employee
	taxRate int
}
// Creamos la interfaz
type PrintInfo interface {
	getMessage() string
}
// Implementamos la interfaz
func (ftEmployee FullTimeEmployee) getMessage() string {
	return "Full Time Employee"
}

func (tEmployee TemporatyEmployee) getMessage() string {
	return "Temporary Employee"
}
// Creamos el metodo de la interfaz
func getMessage(p PrintInfo) {
	fmt.Println(p.getMessage())
}

func main() {
	ftEmployee := FullTimeEmployee{}
	ftEmployee.name = "Benjamin"
	ftEmployee.age = 20
	ftEmployee.id = 1
	fmt.Printf("%v\n", ftEmployee)
	tEmployee := TemporatyEmployee{}
	getMessage(tEmployee)
	getMessage(ftEmployee)
}
```

En lenguajes como Typescript o Java es necesario definir los métodos de la interfaz para poder implementarla y la implementación es explícita.
En cambio en Go, es suficiente definir los métodos para implementar la interfaz y la implementación es implícita.
En el polimorfismo se utiliza una interfaz (o una clase base) para determinar en tiempo de ejecución el método a utilizar, accediéndolo por medio de la interfaz en vez de hacerlo a través de un objeto en particular, porque en este último caso el método se determina en tiempo de compilación (esto no es polimorfismo).

```go
// no se conoce p hasta el momento en que 
// se ejecuta esta función
// podría ser un FullTimeEmployee
// o un TemporaryEmployee
func getMessage(p PrintInfo) {
  fmt.Println(p.getMessage)
}

// aquí ya se conoce el método a usar
// antes de compilar
ftEmployee.getMessage()
```

Entonces con polimorfismo, podrías por ejemplo, estar recibiendo desde una BD o un JSON desde un front, los datos de un empleado en tiempo de ejecución y determinar en ese momento cual es el método apropiado para ese empleado en particular.

## Aplicando interfaces con Abstract Factory

Tengo un aporte-pregunta. Dibujé nuestro Abstract Factory en [UML](https://es.wikipedia.org/wiki/Lenguaje_unificado_de_modelado) 😄 para entender mejor la implementación implicita de `INotificationFactory`.

![AbsFactoryTanx.png](https://static.platzi.com/media/user_upload/AbsFactoryTanx-20cbac36-12fb-42f8-ac11-edd03d458e9c.jpg)

- Patrón de diseño de tipo creacional que permite la producción de objetos de la misma familia o tipo sin especificar su clase correcta, permitiendo esa determinación en tiempo de ejecución

```GO
package main

import "fmt"

// SMS EMAIL

type INotificationFactory interface {
	SendNotification()
	GetSender() ISender
}

type ISender interface {
	GetSenderMethod() string
	GetSenderChannel() string
}

type SMSNotification struct {
}

type SMSNotificationSender struct {
}

func (SMSNotification) SendNotification() {
	fmt.Println("Sending Notification SMS")
}

func (SMSNotification) GetSender() ISender {
	return SMSNotificationSender{}
}

func (SMSNotificationSender) GetSenderMethod() string {
	return "SMS"
}

func (SMSNotificationSender) GetSenderChannel() string {
	return "Twilio"
}
```

## Implementación final de Abstract Factory

ódigo.
Además he añadido una función más que nos permite obtener el canal de envío de nuestros SMS/Emails 😄

```go
package main

import "fmt"

/*
**************************************************
Problema: Enviar notificaciones de Email y SMS.

Métodos/Formas de enviar la notificación:
- Email
- SMS

Canales/Tecnologías para enviar la notificación:
- Twilio (Para SMS)
- SES (Para Email)
**************************************************
*/

// Interfaz encargada enviar una notificación, según un método y un canal de envío.
type INotificationFactory interface {
	SendNotification()
	GetSender() ISender
}

// Interfaz que recoge el método y canal de envío.
type ISender interface {
	GetSenderMethod() string
	GetSenderChannel() string
}

/*
**********************************
	SMS
**********************************
*/
// STRUCT de SMS
type SMSNotification struct {
}

// Método de SMSNotification
func (SMSNotification) SendNotification() {
	fmt.Println("Sending Notification via SMS")
}

// Método de SMSNotification
func (SMSNotification) GetSender() ISender {
	return SMSNotificationSender{}
}

// STRUCT para el envío de notificaciones SMS
type SMSNotificationSender struct {
}

// Método de SMSNotificationSender
func (SMSNotificationSender) GetSenderMethod() string {
	return "SMS"
}

// Método de SMSNotificationSender
func (SMSNotificationSender) GetSenderChannel() string {
	return "Twilio"
}

/*
**********************************
	EMAIL
**********************************
*/
// STRUCT de Email
type EmailNotification struct {
}

// Método de EmailNotification
func (EmailNotification) SendNotification() {
	fmt.Println("Sending Notification via Email")
}

// Método de EmailNotification
func (EmailNotification) GetSender() ISender {
	return EmailNotificationSender{}
}

// STRUCT para el envío de notificaciones Email
type EmailNotificationSender struct {
}

// Método de EmailNotificationSender
func (EmailNotificationSender) GetSenderMethod() string {
	return "Email"
}

// Método de EmailNotificationSender
func (EmailNotificationSender) GetSenderChannel() string {
	return "SES"
}

/*
**********************************
	Funcionalidad
**********************************
*/
// Función que evalua el tipo de notificación que debe enviar
func getNotificationFactory(notificationType string) (INotificationFactory, error) {
	if notificationType == "SMS" {
		return &SMSNotification{}, nil
	}

	if notificationType == "Email" {
		return &EmailNotification{}, nil
	}

	return nil, fmt.Errorf("No Notification Type")
}

// Función que envía la notificación
func sendNotification(f INotificationFactory) {
	f.SendNotification()
}

// Función que escoge la forma/método por la que se enviará la notificación
func getMethod(f INotificationFactory) {
	fmt.Println(f.GetSender().GetSenderMethod())
}

// Función que escoge el canal/tecnología por la que se enviará la notificación
func getChannel(f INotificationFactory) {
	fmt.Println(f.GetSender().GetSenderChannel())
}

func main() {
	smsFactory, _ := getNotificationFactory("SMS")
	emailFactory, _ := getNotificationFactory("Email")

	getMethod(smsFactory)        // Recoge el método de Envío
	getChannel(smsFactory)       // Recoge el canal de Envío
	sendNotification(smsFactory) // Envía la notificación

	getMethod(emailFactory)        // Recoge el método de Envío
	getChannel(emailFactory)       // Recoge el canal de Envío
	sendNotification(emailFactory) // Envía la notificación
}
```

Ejercicio del patrón con unas cuantas modificaciones, espero les sirva, éxitos 😄

https://github.com/juancsr/go-patterns/tree/main/abstract-factory

## Funciones anónimas

Una función anónima es una función definida internamente dentro de un bloque de código, y que no tiene identificador o nombre. Este tipo de funciones no son reutilizables como paquetes, siendo utilizadas únicamente dentro del bloque de código en el que son declaradas.

```go
package main

import (
	"fmt"
	"time"
)

// Funciones anónimas
func main() {
	func() {
		println("Hello")
	}()

	x := 5
	y := func() int {
		return x * 2
	}()
	fmt.Println(y)

	c := make(chan int)
	go func() {
		fmt.Println("Starting function")
		time.Sleep(2 * time.Second)
		fmt.Println("Finishing function")
		c <- 1
	}()
	fmt.Println(<-c)
}
```



## Funciones variadicas y retornos con nombre

Las funciones variadicas nos permiten utilizar como slices los argumentos de funciones de los cuales no sabemos su longitud exacta.
Los retornos con nombre nos permiten definir variables antes de definir el cuerpo de la función, por lo cual utilizaremos return para devolverlos.

Codigo con comentarios y alguna linea extra de ejemplo.

```go
package main

import "fmt"

// recibe un slice de valores enteros y devuelve un entero.
func sum(values ...int) int {
	total := 0
	for _, num := range values {
		total += num
	}

return total

}

// recibe un slice de valores string y los imprime.
func printNames(names ...string) {
	for _, name := range names {
		fmt.Println(name)
	}
}

// Funcion que retorna multiples valores.
func getValues(x int) (double int, triple int, quad int) {
	// return 2 * x, 3 * x, 4 * x // vieja forma.
	double = 2 * x
	triple = 3 * x
	quad = 4 * x
	return
}

func main() {
	// pasando datos en el argumento.
	fmt.Println(sum(3, 5, 7, 10, 25, 4))

// Enviando datos en un arreglo.
numbers := []int{4, 7, 3}
fmt.Println(sum(numbers...))

// Llamando a la funcion y pasando strings
printNames("Carlos", "Jose", "Maria", "Laura")

// Imprimiento multiples returns:
fmt.Println(getValues(5))

// capturando multiples returns:
d, t, q := getValues(5)
fmt.Println(d, t, q)
}
```

# 3. Go Modules

## Cómo utilizar los Go modules

comando que se vieron en la clase:

```bash
# Inicializar un módulo
go mod init github.com/username/module
# Descargar una dependencia
go get github.com/donvito/hellomod
# Limpiar dependencias sin utilizar
go mod tidy
# Información de los módulos cacheados
go mod download -json
```

`main.go`

```go

package main

import (
	"github.com/donvito/hellomod"
    // asignar un alias, limitando conflicto con la primera version.
	hellomod2 "github.com/donvito/hellomod/v2"
)

func main() {
	hellomod.SayHello()
	hellomod2.SayHello("Platzi")
}
```

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - donvito/hellomod: Sample go module to demonstrate new functionality in Go v1.11](https://github.com/donvito/hellomod)

## Creando nuestro módulo

Pueden crear su carpeta de trabajo raíz como si fuera una url to avoid escribir tanto. Ejemplo:

1° directorio: github(.)com
		2° directorios: su username

Aqui hay una pequeña guia para añadir to codigo local/en un repositorio local a [github](https://docs.github.com/en/github/importing-your-projects-to-github/importing-source-code-to-github/adding-an-existing-project-to-github-using-the-command-line)

`main.go`

```sh
# Inicar proyecto
go mod init github.com/nestor94/testingmodule

# Descargar modulo 
go get github.com/nestor84/helloplatzimod
```

`main.go`

```go
package main

// utils alias
import utils "github.com/nestor94/helloplatzimod"

func main() {
    utils.HelloWorld()
}

// Terminal
Hello Wolrd from Platzi!!!
```

# 4. Testing

## Testing

main_test

```go
package main

import "testing"

func TestSum(t *testing.T) {
	tables := []struct {
		x int
		y int
		r int
	}{
		{1, 2, 3},
		{2, 2, 4},
		{3, 2, 5},
		{25, 26, 51},
	}

	for _, table := range tables {
		total := Sum(table.x, table.y)
		if total != table.r {
			t.Errorf("Sum(%d, %d) was incorrect, got: %d, want: %d.", table.x, table.y, total, table.r)
		}
	}
}
```

main

```go
package main

func Sum(x, y int) int {
	return x + y
}

func GetMax(x, y int) int {
	if x > y {
		return x
	}
	return y
}
```

**Testing** 

- Por lo general, muchos lenguajes de programaci&oacute;n utilizan dependencias externas para la creaci&oacute;n y ejecuci&oacute;n de tests.

- Golang, haciendo realce de su magn&iacute;fica librer&iacute;a est&aacute;ndar, nos permite crear los tests que necesitamos para tener un c&oacute;digo m&aacute;s robusto.

## Code coverage

✨ 

Ejecutar los test con la bandera `-coverprofile=coverage.out` para obtener un *coverage*. Esto nos permite saber que parte del código hemos testeado y cual no:

```sh
$ go test -coverprofile=coverage.out
```

Para tener las metricas legibles del resultado del coverage usamos:

```sh
 // ver resumen resumen en la terminal
$ go tool cover -func=coverage.out
// o ver resumen en el navegador
$ go tool cover -html=coverage.out  
```

**Test** - test

```sh
❯ go tool cover -func=coverage.out
github.com/gitlab.com/V-Juarez/testing/main.go:3:       Sum       1
00.0%
github.com/gitlab.com/V-Juarez/testing/main.go:7:       GetMax    0
.0%
total:                                                  (statements
)       25.0%

goIntermedio-Orientado-a-Objetos-Concurrencia/modules/testing on 
main [!?] via 🐹 v1.18
❯
```

**html**

```js

github.com/gitlab.com/V-Juarez/testing/main.go (25.0%)
not tracked not covered covered
package main

func Sum(x, y int) int {
        return x + y
}

func GetMax(x, y int) int  {
        if x > y {
                return x
        }
        return y
}
```

Se puede poner una linea, pero el nombre del archivo debe ir en comillas

```sh
-coverprofile='coverage.out'
-html='coverage.out'
-func='coverage.out'
```

- Code coverage

  Test coverage is a measure used to describe the degree to which the source code of a program is executed when a particular test suite runs.

  If the software you are testing contains a total of 100 lines of code and the number of lines of code that is actually validated in the same software is 50, then the code coverage percentage of this software will be 50 percent.

  To test code coverage you can use:

  ```go
  $ go test -cover
  ```

If you wanted to, you could profile the coverage with:

```go
$ go test -coverprofile=coverage.out # save it
$ go tool cover -func=coverage.out # make it more readable
$ go tool cover -html=coverage.out # or view it on the browser
```

## Profiling

Para ver el uso de CPU del codigo que testeamos, usamos

```sh
$ go test -cpuprofile=cpu.out
```

Para ver el resumen del uso del CPU:

```sh
$ go tool pprof cpu.out
```

Dentro de `pprof` escribimos `top` para ver como se han comportado los programas en nuestro test

```sh
(pprof) top
```

Además, dentro de `pprof` podemos inspeccionar el tiempo promedio de ejecución de cada línea de una función, usando el comando `list <nombre_funcion>`

```go
(pprof) list Fibonacci
```

Tambien podemos ver el reporte del promedio de ejecución:

- en el navegador usando `web`
- o exportarlo en pdf usando `pdf`

```sh
(pprof) web
(pprof) pdf
```

Para salir de `(pprof)` puedes usar `quit` o `Ctrl + D`

> El profiling nos ayudara a encontrar en nuestro codigo aquellas partes criticas que influyen en una ejecucion lenta o con mucho consumo de memoria. Con estas tecnicas sabremos en que enfocarnos a la hora de mejoar nuestro codigo.

[profiling](https://go.dev/blog/pprof)

## Testing usando Mocks

`TestGetFullTimeEmployeeById` 😄

```go
func TestGetFullTimeEmployeeById(t testing.T) {
	table := []struct {
		id               int
		dni              string
		mockFunc         func()
		expectedEMployee FullTimeEmployee
	}{
		{
			id:  1,
			dni: "1",
			mockFunc: func() {
				GetEmployeeById = func(id int) (Employee, error) {
					return Employee{Id: 1, Position: "CEO"}, nil
				}

				GetPersonByDNI = func(id string) (Person, error) {
					return Person{Name: "Andy", Age: 35, DNI: "1"}, nil
				}
			},
			expectedEMployee: FullTimeEmployee{},
		},
	}
}
```

test

```go
package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestGetFullTimeEmployeeById(t *testing.T) {
	type args struct {
		id  int
		dni string
	}
	tests := []struct {
		name     string
		args     args
		mockFunc func()
		want     FullTimeEmployee
		wantErr  bool
	}{
		// TODO: Add test cases.
		{name: "Test1",
			args: args{id: 1, dni: "552"},
			mockFunc: func() {
				GetEmployeeById = func(id int) (Employee, error) {
					return Employee{
						Id:       1,
						Position: "CEO",
					}, nil
				}

				GetPersonByDNI = func(dni string) (Person, error) {
					return Person{
						DNI:  "552",
						Name: "Jhon",
						Age:  38,
					}, nil
				}
			},
			want: FullTimeEmployee{
				Person: Person{
					DNI:  "552",
					Name: "Jhon",
					Age:  38,
				},
				Employee: Employee{
					Id:       1,
					Position: "CEO",
				},
			},
			wantErr: false},
		{name: "Test2",
			args: args{id: 2, dni: "96D"},
			mockFunc: func() {
				GetEmployeeById = func(id int) (Employee, error) {
					return Employee{
						Id:       2,
						Position: "Manager",
					}, nil
				}

				GetPersonByDNI = func(dni string) (Person, error) {
					return Person{}, fmt.Errorf("Person not found")
				}
			},
			want: FullTimeEmployee{
				Person: Person{
					DNI:  "96D",
					Name: "Marta",
					Age:  29,
				},
				Employee: Employee{
					Id:       2,
					Position: "Manager",
				},
			},
			wantErr: true},
	}

	originalGetEmployeeById := GetEmployeeById
	originalGetPersonByDNI := GetPersonByDNI

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mockFunc()

			got, err := GetFullTimeEmployeeById(tt.args.id, tt.args.dni)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFullTimeEmployeeById() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !tt.wantErr {
				if !reflect.DeepEqual(got, tt.want) {
					t.Errorf("GetFullTimeEmployeeById() = %v, want %v", got, tt.want)
				}
			}
		})
	}

	GetEmployeeById = originalGetEmployeeById
	GetPersonByDNI = originalGetPersonByDNI

}
```

## Implementando Mocks

Hay una librería llamada Mockery, que te genera los Mocks automáticamente utilizando un comando.

Una vez te genera un mock, es llegar y utilizarlo. Muy fácil y practico.
Dejo el proyecto para que le echen una mirada: [mockery](https://github.com/vektra/mockery)

> Es muy importante guardar los valores originales de las funciones que estamos utilizando para realizar nuestros tests en caso de que en test posteriores decidamos evaluar otras partes del codigo que tambien dependen de esas funciones.

# 5. Concurrencia

## Unbuffered channels y buffered channels

**Unbuffered channel:** Espera una función o una rutina para recibir el mensaje, es bloqueada por ser llamada en la misma función

**Buffered channel:** Se puede llamar de manera inmediata, en el siguiente ejemplo ***2*** es el numero de canales que pueden ser usados

```go
package main

import "fmt"

func main() {
  // c := make(chan int) // Unbuffered
  c := make(chan int, 2) // Buffered

  c <- 1
  c <- 2

  fmt.Println(<-c)
  fmt.Println(<-c)
}
```

> An unbuffered channel is used to perform synchronous communication between goroutines while a buffered channel is used for perform asynchronous communication. An unbuffered channel provides a guarantee that an exchange between two goroutines is performed at the instant the send and receive take place.

```go
package main

import "fmt"

func main() {
	// Unbuffered channel
	ch := make(chan int)
	go func() {
		ch <- 1
	}()
	fmt.Println(<-ch)

	// Buffered channel
	ch2 := make(chan int, 1)
	ch2 <- 2
	fmt.Println(<-ch2)

	ch2 <- 3
	fmt.Println(<-ch2)

}
```

## Waitgroup

Codigo con comentarios:

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

/*
creates a wait group that gets incremented by on on every iteration, which
will as well run the doSmth() func that will substract 1 from the wait group
after it finishes. wg.Wait() at the end guarantees that it'll wait for the wg (counter)
to be 0.
*/

func doSmth(u int, wg *sync.WaitGroup) {
	defer wg.Done()

	fmt.Printf("Started at #%d\n", u)
	time.Sleep(time.Second * 2)
	fmt.Println("Ended...")
}

func main() {

	var wg sync.WaitGroup

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go doSmth(i, &wg)
	}

	wg.Wait()
}
```

Un WaitGroup espera que una colección de gorutinas terminen su trabajo. La gorutina de main llama Add para configurar el número de gorutinas por las que tiene que esperar. Luego cada una de las gorutinas corre y llama a Done cuando terminan. Al mismo tiempo, Wait puede ser usado para bloquear hasta que todas las gorutinas hayan finalizado. Escribir código concurrente es súper fácil: todo lo que tenemos que hacer es poner “go” en frente de una llamada a una función o método.

```go
runtime.NumCPU()
runtime.NumGoroutine()
sync.WaitGroup
func (wg *WaitGroup) Add(delta int)
func (wg *WaitGroup) Done()
func (wg *WaitGroup) Wait()
```

[`WaitGroup` 😄](https://golang.org/pkg/sync/#WaitGroup)

## Buffered channels como semáforos

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup
	c := make(chan int, 5)

	for i := 0; i < 10; i++ {
		wg.Add(1)
		c <- 1
		go doSomething(i, &wg, c)
	}

	fmt.Println("Waiting for goroutines to finish")

	wg.Wait()

}

func doSomething(i int, wg *sync.WaitGroup, c chan int) {
	defer wg.Done()

	fmt.Printf("id: %d started \n", i)
	time.Sleep(4 * time.Second)
	fmt.Printf("id: %d finished \n", i)

	<-c
}
```

Code with comments:

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

/*
traffic light.

this uses channels and waint groups to 1. execute only 2 doSmth() func
at a time and 2. be able to wait for all of them.

in order of execution it'll:
c := [][] -- two free spaces
c := [routine][] -- one free space
c := [rountine][routine] -- all occupied
c := [][routine] -- one free space
*/

func doSmth(i int, wg *sync.WaitGroup, c chan int) {
	defer wg.Done()
	fmt.Printf("Id: %d -> started...\n", i)
	time.Sleep(time.Second * 4)
	fmt.Printf("Id: %d -> finished...\n", i)

	<- c // frees the space for new routines
}

func main() {
	c := make(chan int, 2) // creates a buffered channel with a capacity of two
	var wg sync.WaitGroup // creates wait group

	for i := 0; i < 10; i++ {
		c <- 1 // alocate a new "instance" in the free space
		wg.Add(1) // adds to the wait group
		go doSmth(i, &wg, c)
	}

	wg.Wait()
}
```

## Definiendo channels de lectura y escritura

> Cuando se trabaja con channels existe la gran probabilidad de crear un deadlock si no somos cuidadosos con su utilizacion, una forma de mitigar parte de este riesgo es definiendo canales de lectura o escritura, pero no ambos.

- Canal de solo escritura: Flecha <- a la derecha de chan

```go
func Generator(c chan<- int)
```

- Canal de solo lectura: Flecha <- a la izquierda de chan

```go
func Print(c <-chan int)
```

![tipGo.png](https://static.platzi.com/media/user_upload/tipGo-f412d8de-7fbe-4634-b363-ba8ffd8c8815.jpg)

```go
package main

// Generator generates a stream of integers
func Generator(c chan<- int) {
	for i := 1; i <= 10; i++ {
		c <- i
	}
	close(c)
}

// Double doubles the value received from the input channel
func Double(in <-chan int, out chan<- int) {
	for i := range in {
		out <- i * 2
	}
	close(out)
}

// Print prints the values received from the input channel
func Print(c <-chan int) {
	for i := range c {
		println(i)
	}
}

// main is the entry point for the program
func main() {
	// Create new channels
	in := make(chan int)
	out := make(chan int)

	// Launch the goroutines
	go Generator(in)
	go Double(in, out)
	Print(out)
}
```

## Worker pools

Worker pools are a model in which a fixed number of m workers (implemented in Go with goroutines) work their way through n tasks in a work queue (implemented in Go with a channel). Work stays in a queue until a worker finishes up its current task and pulls a new one off.

Basically there are a set amount of workers and n tasks. Every worker takes a task, and when it’s done it takes another one. This is very useful for breakable task that require a lot of processing.

This is an example program:

```go
package main

import "fmt"

func Worker(id int, jobs <- chan int, results chan <- int) {
	for job := range jobs {
		fmt.Printf("--> Worker #%d started fib with %d\n", id, job)
		fib := Fibo(job)
		fmt.Printf("Worker #%d finished. Job: %d; Fibo: %d\n", id, job, fib)
		results <- fib
	}
}

func Fibo(n int) int {
	if n <= 1 {
		return n
	}

	return Fibo(n - 1) + Fibo(n - 2)
}

func main() {
	tasks := []int{2, 3, 4, 7, 10, 15}
	nWorkers := 3
	jobs := make(chan int, len(tasks))
	results := make(chan int, len(tasks))

	// creates the workers
	for i := 0; i < nWorkers; i++ {
		go Worker(i, jobs, results)
	}

	// sends them to work
	for _, v := range tasks {
		jobs <- v
	}

	// close channel to indicate that's all the work to do
	close(jobs)

	for i := 0; i < len(tasks); i++ {
		<- results
	}
}
```

code 

```go
package main

import "fmt"

// Worker is a function that does the work.
func Worker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		fmt.Println("worker", id, "started  job", j)
		fib := Fibonnaci(j)
		results <- fib
		fmt.Println("worker", id, "finished job", j, "result", fib)
	}
}

// Fibonacci returns the nth number in the Fibonacci sequence.
func Fibonnaci(n int) int {
	if n < 2 {
		return n
	}
	return Fibonnaci(n-1) + Fibonnaci(n-2)
}

func main() {
	// tasks to do
	tasks := []int{2, 3, 4, 5, 7, 10, 12, 35, 37, 40, 41, 42}

	nWorkers := 5
	jobs := make(chan int, len(tasks))
	results := make(chan int, len(tasks))

	// start the workers
	for w := 1; w <= nWorkers; w++ {
		go Worker(w, jobs, results)
	}

	// give the workers jobs
	for _, t := range tasks {
		jobs <- t
	}
	close(jobs)

	// get the results (consume the channel)
	for a := 1; a <= len(tasks); a++ {
		<-results
	}
}
```



### Explanation

https://gobyexample.com/worker-pools

## Multiplexación con Select y Case

Code:

```go
package main

import (
	"fmt"
	"time"
)

func doSomething(i time.Duration, c chan<- int, param int) {
	time.Sleep(i)
	c <- param
}

func main() {
	c1 := make(chan int)
	c2 := make(chan int)

	d1 := 4 * time.Second
	d2 := 2 * time.Second

	go doSomething(d1, c1, 1)
	go doSomething(d2, c2, 2)

	for i := 0; i < 2; i++ {
		select {
		case channelMsg1 := <-c1:
			fmt.Println(channelMsg1)
		case channelMsg2 := <-c2:
			fmt.Println(channelMsg2)
		}
	}
}
```

> Cuando una rutina se esta comunicando con varios channels es muy util utilizar la palabra reservada select para poder interactuar de una manera mas ordenada con todos los mensajes que estan siendo recibidos.

[Ejemplo de select y case](https://tour.golang.org/concurrency/5)

# 6. Proyecto: servidor con worker pools

## Definiendo workers, jobs y dispatchers

comentarios:

```go
package main

import (
	"fmt"
	"time"
)

// Job represents a job to be executed, with a name and a number and a delay
type Job struct {
	Name   string        // name of the job
	Delay  time.Duration // delay between each job
	Number int           // number to calculate on the fibonacci sequence
}

// Worker will be our concurrency-friendly worker
type Worker struct {
	Id         int           // id of the worker
	JobQueue   chan Job      // Jobs to be processed
	WorkerPool chan chan Job // Pool of workers
	Quit       chan bool     // Quit worker
}

// Dispatcher is a dispatcher that will dispatch jobs to workers
type Dispatcher struct {
	WorkerPool chan chan Job // Pool of workers
	MaxWorkers int           // Maximum number of workers
	JobQueue   chan Job      // Jobs to be processed
}

// NewWorker returns a new Worker with the provided id and workerpool
func NewWorker(id int, workerPool chan chan Job) *Worker {
	return &Worker{
		Id:         id,
		WorkerPool: workerPool,
		JobQueue:   make(chan Job),  // create a job queue
		Quit:       make(chan bool), // Channel to end jobs
	}
}

// Start method starts all workers
func (w Worker) Start() {
	go func() {
		for {
			w.WorkerPool <- w.JobQueue // add job to pool

			// Multiplexing
			select {
			case job := <-w.JobQueue: // get job from queue
				fmt.Printf("worker%d: started %s, %d\n", w.Id, job.Name, job.Number)
				fib := Fibonacci(job.Number)
				time.Sleep(job.Delay)
				fmt.Printf("worker%d: finished %s, %d with result %d\n", w.Id, job.Name, job.Number, fib)
			case <-w.Quit: // quit if worker is told to do so
				fmt.Printf("Worker with id %d Stopped\n", w.Id)
				return
			}
		}
	}()
}

// Stop method stop the worker
func (w Worker) Stop() {
	go func() {
		w.Quit <- true
	}()
}

// Fibonacci calculates the fibonacci sequence
func Fibonacci(n int) int {
	if n <= 1 {
		return n
	}
	return Fibonacci(n-1) + Fibonacci(n-2)
}

// NewDispatcher returns a new Dispatcher with the provided maxWorkers
func NewDispatcher(jobQueue chan Job, maxWorkers int) *Dispatcher {
	pool := make(chan chan Job, maxWorkers)
	return &Dispatcher{
		WorkerPool: pool,
		MaxWorkers: maxWorkers,
		JobQueue:   jobQueue,
	}
}

// Dispatch will dispatch jobs to workers
func (d *Dispatcher) dispatch() {
	for {
		select {
		case job := <-d.JobQueue: // get job from queue
			// Asign the job to a worker
			go func() {
				jobChannel := <-d.WorkerPool // get worker from pool
				jobChannel <- job            // Workers will read from this channel
			}()
		}
	}
}

func (d *Dispatcher) Run() {
	for i := 0; i < d.MaxWorkers; i++ {
		worker := NewWorker(i+1, d.WorkerPool)
		worker.Start()
	}

	go d.dispatch()
}
```

> Golang permite utilizar conceptos como los de worker pools y, en combinacion con buffered channels, la creacion de job queues que utilizando concurrencia nos permitiran tener un alto rendimiento a la hora de la creacion de muchas tareas.

## Creando web server para procesar jobs

> Con la libreria estandar de Go y utilizando el paquete net, somos capaces de crear un servidor que sera el que atienda las peticiones y asignara los nuevos workers para que lleven a cabo los trabajos que es esta buscando conseguir.

- El Dispacher obtendra una cola, que contedra una cola de cada uno de los workers.
- Una vez que el dispacher reciba un trabajo, esta agarrara de su cola a un worker, y mandara la tarea por ese medio
- El worker, procesara el trabajao y volvera a agregar su cola de trabajo a la cola del dispacher

Una manera mas simple de cumplir la interface de http es retornar una handlerFunc en nuestra funcion, con esto podemos recibir multiples parametros y quedara una sintaxis mas limpia

```go
func RequestHandler(jobQueue chan Job) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request)
```

y al momento de llamarla en main quedaria de esta manera:

```go
http.HandleFunc("/fib", RequestHandler(jobQueue))
```

![dispacher.jpg](https://static.platzi.com/media/user_upload/dispacher-afa550f5-204a-4ed2-8e25-61103fb8e090.jpg)

# 7. Conclusión

## Continúa con el Curso de Go Avanza

> Eres capaz de crear tests unitarios para tu codigo, analizar las partes criticas que podrias mejorar y tener una metrica de que codigo necesitas testear para comprobar la existencia de bugs.

Tines conocimientos de lso diferentes tipos de Channels y lo que involucra cada uno, errores comunues que podrias enfretar como ser Deadlocks y utilizarlos para crear semaforos a la hora de ejecutar GoRoutimes.

- Has aprendido nuevas tenicas para la concurerenci; como usear multiplexacion, worker pools y waitgroups.
- Has creado una jov queue concurrente que es capaz de ejecutar multiples tareas utilizando GoRoutimes.

Go, test y POO