<h1>Programación en Go</h1>

<h3>Osmandi Gómez</h3

<h1>Tabla de Contenido</h1>

- [1. Hola mundo en Go](#1-hola-mundo-en-go)
  - [Introducción al Curso de Golang](#introducción-al-curso-de-golang)
  - [¿Qué es, por qué y quienes utilizan Go?](#qué-es-por-qué-y-quienes-utilizan-go)
  - [Instalar Go en Linux](#instalar-go-en-linux)
  - [Instalar Go en Windows](#instalar-go-en-windows)
    - [Descarga de Go](#descarga-de-go)
    - [Instalación](#instalación)
    - [Pasos finales](#pasos-finales)
  - [Instalar Go en Mac](#instalar-go-en-mac)
    - [Instalar Go en Mac](#instalar-go-en-mac-1)
    - [Instalación](#instalación-1)
    - [Pasos Finales](#pasos-finales-1)
  - [Nuestras primeras líneas de código en Go](#nuestras-primeras-líneas-de-código-en-go)
- [2. Variables, funciones y documentación](#2-variables-funciones-y-documentación)
  - [Variables, constantes y zero values](#variables-constantes-y-zero-values)
  - [Operadores aritméticos](#operadores-aritméticos)
  - [Tipos de datos primitivos](#tipos-de-datos-primitivos)
  - [Paquete fmt: algo más que imprimir en consola](#paquete-fmt-algo-más-que-imprimir-en-consola)
    - [Tipos de Print:](#tipos-de-print)
    - [Imprimir el tipo de dato:](#imprimir-el-tipo-de-dato)
  - [Uso de funciones](#uso-de-funciones)
  - [Go doc: La forma de ver documentación](#go-doc-la-forma-de-ver-documentación)
- [3. Estructuras de control de flujo y condicionales](#3-estructuras-de-control-de-flujo-y-condicionales)
  - [El poder de los ciclos en Golang: for, for while y for forever](#el-poder-de-los-ciclos-en-golang-for-for-while-y-for-forever)
  - [Operadores lógicos y de comparación](#operadores-lógicos-y-de-comparación)
  - [El condicional if](#el-condicional-if)
  - [Múltiple condiciones anidadas con Switch](#múltiple-condiciones-anidadas-con-switch)
  - [El uso de los keywords defer, break y continue](#el-uso-de-los-keywords-defer-break-y-continue)
- [4. Estructuras de datos básicas](#4-estructuras-de-datos-básicas)
  - [Arrays y Slices](#arrays-y-slices)
  - [Recorrido de Slices con Range](#recorrido-de-slices-con-range)
  - [Llave valor con Maps](#llave-valor-con-maps)
  - [Structs: La forma de hacer clases en Go](#structs-la-forma-de-hacer-clases-en-go)
  - [Modificadores de acceso en funciones y Structs](#modificadores-de-acceso-en-funciones-y-structs)
- [5. Métodos e interfaces](#5-métodos-e-interfaces)
  - [Structs y Punteros](#structs-y-punteros)
  - [Stringers: personalizar el output de Structs](#stringers-personalizar-el-output-de-structs)
  - [Interfaces y listas de interfaces](#interfaces-y-listas-de-interfaces)
- [6. Concurrencia y Channels](#6-concurrencia-y-channels)
  - [¿Qué es la concurrencia?](#qué-es-la-concurrencia)
  - [Primer contacto con las Goroutines](#primer-contacto-con-las-goroutines)
  - [Channels: La forma de organizar las goroutines](#channels-la-forma-de-organizar-las-goroutines)
  - [Range, Close y Select en channels](#range-close-y-select-en-channels)
- [7. Manejo de paquetes y Go Modules](#7-manejo-de-paquetes-y-go-modules)
  - [Go get: El manejador de paquetes](#go-get-el-manejador-de-paquetes)
  - [Go modules: Ir más allá del GoPath con Echo](#go-modules-ir-más-allá-del-gopath-con-echo)
  - [Modificando módulos con Go](#modificando-módulos-con-go)
- [8. Despedida del curso](#8-despedida-del-curso)
  - [Despedida](#despedida)
- [9. Bonus](#9-bonus)
  - [Cheat Sheet Go](#cheat-sheet-go)
  - [Data Science con Go](#data-science-con-go)
  - [Librerías para desarrollo web con Go](#librerías-para-desarrollo-web-con-go)

# 1. Hola mundo en Go

## Introducción al Curso de Golang

- [slides-programacion-go.pdf](https://drive.google.com/file/d/1OIblB8MM3oDtWsY0w2aYTxU-oxt_4wXK/view?usp=sharing)

## ¿Qué es, por qué y quienes utilizan Go?

**Qué es?**

  - Lenguaje compilado (se recopilan los códigos) y estáticamente tipado (se debe indicar el tipo de variable o constante para que guarde algún valor en él)
  - Maneja procesos pesados, es potente, pero amigable.
  - Se utiliza Go/Goland para nombrarlo.
  - Los programadores de este lenguaje se hacen llamar gophers.

**¿Porqué Go/Goland?**

  - Es veloz
  - Tiene alto rendimiento para tareas pesadas
  - Maneja soporte nativo por concurrencia
  - Un Gopher puede ganar $74k al año
  - Facilita ajustar sintaxis de forma nativa
  - Comunidad receptiva, contribuye y apoya.

**Go / Golang:**

  - Compilado y tipado estático
  - Potencia de C y sintaxis amigable como Python.
  - Gran velocidad de compilación.
  - Alto rendimiento (utiliza todos los cores)
  - Soporte nativo para concurrencia.

**¿Dónde se usa?**

  - Mercado Libre
  - Twich
  - Twitter
  - Uber
  - Docker y Kubernetes

- [Gopherize.me - A Gopher pic that's as unique as you](https://gopherize.me/)
- [Go's New Brand - The Go Blog](https://blog.golang.org/go-brand)
- [Go logo design process - YouTube](https://youtu.be/V4t-ymImW6c)
- [Stack Overflow Developer Survey 2020](https://insights.stackoverflow.com/survey/2020)
- [GoUsers · golang/go Wiki · GitHub](https://github.com/golang/go/wiki/GoUsers)

## Instalar Go en Linux


- [Downloads - The Go Programming Language](https://golang.org/dl/)

colocar o en el `.bashrc` o el `.zshrc` pero si no quieres complicarte lo puedes hacer en el `.profile`

``` GO
# Go Path
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export GOROOT=/usr/local/go

export PATH=$PATH:$GOBIN:$GOROOT/bin
```

**Resumen**

1. Descargar la ultima versión de go desde su pagina oficial https://golang.org/dl/

2. Descomprimir el archivo `tar.gz `en el directorio `/usr/local`

3. En home modificar el archivo `.bashrc` con visual studio code o cualquier otro editor de texto. Añadiremos las siguientes líneas al final del archivo:

``` GO
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export GOROOT=/usr/local/go

export PATH=$PATH:$GOBIN:$GOROOT/bin
```

  nota: `$HOME= /home/nombre-del-usuario`

4. para ejecutar los cambios realizados usa `. .bashrc` en el home o también funciona abrir una nueva terminal

5. verificar que go este funcionando correctamente con `go version`

6. en nuestro home crear la carpeta go y dentro de estas las subcarpetas `bin pkg src`

## Instalar Go en Windows

Para desarrollo de Go en Windows te recomendaría que uses WSL (Windows Subsystem Linux) ya que Go tiene un mejor desempeño en Linux que en el mismo Windows. El proceso de instalación son los mismos pasos que en el de Linux.

Sin embargo, igualmente te compartiré cómo instalarlo en este Windows.

### Descarga de Go

El primer paso es descargar Go para windows desde este [enlace](https://golang.org/dl/)eligiendo la opción Microsoft Windows.

### Instalación

Al archivo que descargaste, solo debes ejecutarlo e iniciar la instalación. Por defecto, éste se instalaría en C:\Go.

Una vez instalado, entra a la carpeta donde instalaste Go y dentro crea las siguientes tres carpetas: bin, pkg y src

El paso siguiente es crear las variables de entorno GOPATH y GOBIN.

En cuanto al GOBIN, debes agregarlo también al Path para que así cuando instales CLIs (Command Line Interface) de Go éstas puedan ejecutarse en cualquier parte de la terminal.

### Pasos finales

Una vez hayas ejecutado los pasos anteriores, solo debes abrir la consola de tu Windows, puede ser CMD o PowerShell. Y colocal el siguiente comando: go version

Si imprime la versión de Go, significa que la instalación ha sido exitosa.

El paso final es que instales tu editor de código favorito, en el curso estaremos usando [VS Code](https://code.visualstudio.com/).

¡Y listo! Ya estamos listos(as) para empezar.

## Instalar Go en Mac

### Instalar Go en Mac
El proceso de instalación de Go en Mac es muy similar al proceso de instalación en Linux. Pero, a diferencia que en Linux, en Mac el paquete Go suele estar muy actualizado respecto a la última versión estable.

### Instalación

**Método 1:**

Para este caso, usaremos el manejador de paquetes 

```bash
brew
```
 que es muy popular para la programación en macOS.

Para instalar Go, solo debes ingresar en la terminal el siguiente comando 

``` BASH
brew install golang
```

**Método 2:**

Debes irte a la página de [descarga de Go](https://golang.org/dl/), y seleccionar macOS.

Una vez descargado el paquete, debes abrirlo y luedo de seguir los pasos del asistente instalarás Go en tu mac que por defecto se instalará en la siguiente ruta `/usr/local/go`

Si todo ha salido bien, al abrir una terminal y ejecutar 

``` BASH
go version 
```

debería imprimirte en consola la versión de Go que acabas de instalar.

### Pasos Finales

Una vez tengas instalado Go, el paso siguiente es crear la carpeta go, preferiblemente en tu $HOME.

Una vez creada, debes igualmente crear dentro de ella las siguientes tres carpetas: pkg, src y bin.

Para finalizar, el paso siguiente es crear las variables de entorno. Esto dependerá mucho de cuál shell estés utilizando. En el curso estaremos usando Bash Shell, en este caso abrimos el archivo `~/.bash_profile` y allí escribimos lo siguiente al final del archivo:

``` GO
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOBIN
```

El paso final es que instales tu editor de código favorito, en el curso estaremos usando VS Code.

¡Y listo! Ya estamos listos(as) para empezar.

## Nuestras primeras líneas de código en Go

```Go
package main    // NOmbre del archivo de la carpeta main.

import "fmt"

func main() {
	fmt.Println("Hello, World")
}
```

Compilar el codigo
```BASH
go build src/main.go
```

Ejecutar sin Compilar
```BASH
go run src.main.go
```

- [GitHub - osmandi/curso_golang_platzi: Código del Curso de programación básica en Go en Platzi](https://github.com/osmandi/curso_golang_platzi)

# 2. Variables, funciones y documentación

## Variables, constantes y zero values

```Go
// Go
func main() {
	//Declaracion de constantes
	const pi float64 = 3.14
	const pi2 = 3.16

	fmt.Println("Pi:", pi)
	fmt.Println("Pi2:", pi2)

	//Declaracion de varaibles
	base := 12          //Primera forma
	var altura int = 14 //Segunda forma
	var area int        //Go no compila si las variables no son usadas

	fmt.Println(base, altura, area)

	//Zero values
	//Go asigna vaalores a variables vacías
	var a int
	var b float64
	var c string
	var d bool

	fmt.Println(a, b, c, d)

	//Ejercicio
	//Calcular el áera del cuadrado
	const baseCuadrado = 10
	areaCuadrado := baseCuadrado * baseCuadrado

	fmt.Println("El área del cuadrado es:", areaCuadrado)
}
```

Zero values: valores por defecto que toman las variables al ser declaradas pero sin asignación de valor inicial.

```bash
int: 0
float: 0
string: “” (empty string)
bool: false
```

constants
const <name> <type> = <value>

variables
var <name> <type> = <value>
or
<name> <type> := <value>

Zero values
Default values, not null, when no values assigned to variables nor constants.

## Operadores aritméticos

```Go
// Go
package main

import "fmt"

func main()  {

	/* Retos
	Calcular el área de:
	Rectángulo
	Trapecio
	Círculo
	*/

	/* Aŕea del Rectángulo
		Fórmula = b*h
	*/
	baseRectangulo := 6
	alturaRectangulo := 10

	fmt.Println("Área Rectángulo:",baseRectangulo*alturaRectangulo)

	/* Aŕea del Trapecio
		Fórmula = h * a+b/2
	*/
	alturaTrapecio := 5
	aTrapecio := 3
	bTrapecio := 8
	areaTrapecio := alturaTrapecio * (aTrapecio+bTrapecio)/2
	fmt.Println("Área Trapecio:",areaTrapecio)

	/* Aŕea del Círculo 
		Fórmula = pi * r**2
	*/
	pi := 3.1416
	radioCirculo := 2
	areaCirculo := pi * (float64(radioCirculo)*float64(radioCirculo))
	fmt.Println("Área Círculo:",areaCirculo)
}
```

## Tipos de datos primitivos

  - `int8/16/32/64:` indica el tipo de dato “int” con signo, y además el tamaño del dato.
  - `uint8/16/32/64:` indica el tipo de dato “int” pero sin signo(solo positivos) y además el tamaño máximo del dato
  - `int / uint :` toma el tamaño de bits en el que está basado el procesador (con signo / sin signo)
  - `float32:` 32 bits con signo
  - `float64:` 64 bits con signo
  - `string:` se deben declarar con comillas dobles “”
  - `bool:` true o false
WOW
  - `complex64:` números complejos (real e imaginario float32)
  - `complex128:` números complejos (real e imaginario float64)

Ejemplo de implementación de la mayoría de tipos:

```Go
// Go
//Data Types
	var shortInt int8 = 3
	var longInt int64 = 2313212113234256876
	var shortFloat float32 = 3.1
	var longFloat float64 = 3.153465212456432145668723312
	var text string = "string"
	var boolean bool = true
	var complex complex128 = 10 + 8i
```

## Paquete fmt: algo más que imprimir en consola


**fmt**
El paquete fmt es el que se encarga de administrar los inputs y outputs de la terminal.

### Tipos de Print:

- `Println:` Es un print normal con un salto de linea al final. Ejemplo:

``` Go
fmt.Println("Hola Mundo")
```

- `Printf:` Es un print al cual le puedes especificar el tipo de objeto que le vas a dar. Ejemplo:

``` Go
fmt.Printf("%s tiene más de %d cursos\n", nombre, cursos)
```

- `Sprintf:` No imprime nada en consola, simplemente lo guarda como un String. Ejemplo de uso:

``` Go
var message string = fmt.Sprintf("%v tiene más de %v cursos\n", nombre, cursos)

fmt.Println(message)
```
### Imprimir el tipo de dato:

Con este paquete podemos imprimir en consola el tipo de dato de variables o constantes. Ejemplo de uso:

``` GO
package main

import "fmt"

func main() {

	const nombre string = "UltiRequiem"

	fmt.Printf("La variable 'nombre' es de tipo : %T\n", nombre)
}
```
- [fmt - The Go Programming Language](https://golang.org/pkg/fmt/)

## Uso de funciones

Ejemplo de funciones:
```Go

package main

import "fmt"

func firstFunction(message string) {
	fmt.Println(message)
}

func threeArguments(first, second int, third string) {
	fmt.Printf("Two numbers: %d and %d. One string: %s\n", first, second, third)
}

func returningFunction(number int) int {
	return number * 2
}

func doubleReturn(number int) (c, d int) {
	return number, number * 2
}

func main() {
	fmt.Println("Function program initialized!")

	message := "First message in a function example"
	firstFunction(message)

	threeArguments(15, 7, "String!")

	fmt.Println(returningFunction(4))

	num, twiceNum := doubleReturn(2)
	fmt.Printf("%d multiplied by 2 is: %d\n", num, twiceNum)

	_, twiceEight := doubleReturn(8)
	fmt.Printf("8 multiplied by 2 is: %d\n", twiceEight)
}
```


## Go doc: La forma de ver documentación

- [Packages - The Go Programming Language](https://golang.org/pkg/)
- [Home · pkg.go.dev](https://godoc.org/)

# 3. Estructuras de control de flujo y condicionales

## El poder de los ciclos en Golang: for, for while y for forever

CODIGO:

```go
package main import "fmt" func main() { 	// for contrario 	for i := 10; i > 0; i-- { 		fmt.Println(i) 	} }
```

RESULTADO:

```bash
10
9
8
7
6
5
4
3
2
1
```

```go
package main

import "fmt"

func main() {

	// For condicional
	for i := 0; i < 11; i++ {
		fmt.Println(i)
	}

	fmt.Printf("\n")

	// For while
	counter := 0
	for counter < 10 {
		fmt.Println(counter)
		counter++
	}

	// For forever
	counterForever := 0
	for {
		fmt.Println(counterForever)
		counterForever++
	}
}
```

## Operadores lógicos y de comparación

Son operadores que nos permiten hacer una comparación de condiciones y en caso de cumplirse como sino ejecutarán un código determinado. Si se cumple es VERDADERO/TRUE y si no se cumple son FALSO/FALSE.

Empecemos con los operadores de comparación:

## Operadores de comparación

Son aquellos que retornan TRUE o FALSE en caso de cumplirse o no una expresión. Son los siguientes:

- *valor1 == valor2*: Retorna TRUE si valor1 y valor2 son exactamente iguales.
- *valor1 != valor2*: Retorna TRUE si valor1 es diferente de valor2.
- *valor1 < valor2*: Retorna TRUE si valor1 es menor que valor2
- *valor1 > valor2*: Retorna TRUE si valor1 es mayor que valor2
- *valor1 >= valor2*: Retorna TRUE si valor1 es igual o mayor que valor2
- *valor1 <= valor2*: Retorna TRUE si valor1 es menor o igual que valor2.

## Operadores lógicos

Son aquellos que retorna TRUE o FALSE si cumplen o no una condición utilizando [puertas lógicas](https://platzi.com/clases/1050-programacion-basica/15968-que-son-tablas-de-verdad-y-compuertas-logicas/).

### Operador AND:

Este operador indica que todas las condiciones declaradas deben cumplirse para poderse marcar como TRUE. En Go, se utiliza este símbolo `&&`.

Ejemplo1: `1>0 && 2 >0` Esto retornará TRUE porque tanto la primera como la segunda condición son verdaderas.

Ejemplo2: `2<0 && 1 > 0` Esto retornará FALSE porque una de las condiciones no es verdadera.

### Operador OR:

Este operador indica que al menos una de las condiciones debe cumplirse para marcarse como TRUE. En Go, se representa con el símbolo `||`.

Ejemplo: `2<0 || 1 > 0` Esto retornará TRUE porque la segunda condición se cumple, a pesar que la primera no.

### Operador NOT:

Este operador retornará el opuesto al boleano que está dentro de la variable. Ejemplo:

```Go
myBool :=  true
fmt.Println(!myBool) // Esto retornará false
```

------

Una vez ya estudiado la teoría, en la siguiente clase vamos a ver cómo utilizarlo con más detalles en Go.

## El condicional if

Reto 1, escribiendo input desde consola

```go
var number int

	fmt.Println("Write any number")
	fmt.Scan(&number)

	numberType := number % 2

	if numberType == 0 {
		fmt.Printf("%d is even", number)
	} else {
		fmt.Printf("%d is odd", number)
	}
```

Reto 2, ingresando datos desde consola

```go
defaultUser := "admin"
	defaultPassword := "12345"
	var inputUser string
	var inputPassword string

	fmt.Print("Username: ")
	fmt.Scanln(&inputUser)
	fmt.Print("Password: ")
	fmt.Scanln(&inputPassword)

	if inputUser == defaultUser && inputPassword == defaultPassword {
		fmt.Print("Success login")
	} else {
		fmt.Print("Failed login")
	}
```

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[strconv - The Go Programming Language](https://golang.org/pkg/strconv/)

## Múltiple condiciones anidadas con Switch

```go
package main

import "fmt"

func parOImpar(num int) string{
	switch modulo := num % 2; modulo {
	case 0:
		return "Es par"
	default:
		return "Es impar"
	}
}

func main() {
	valor1 := 1

	if valor1 == 1 {
		fmt.Println("Es 1")
	} else {
		fmt.Println("No es 1")
	}

	queEs := parOImpar(24)
	fmt.Println(queEs)

	value := 50
	switch {
	case value > 100:
		fmt.Println("Es mayor que 100")
	case value < 0:
		fmt.Println("Es menor a 0")
	default:
		fmt.Println("No condicion")
	}
}
```

### Switch

```go
package main

import "fmt"

func main() {

	switch modulo := 5 % 2; modulo {
	case 0:
		fmt.Println("Es par")
	default:
		fmt.Println("Es impar")
	}

	// Sin condicion
	value := 200
	switch {
	case value > 100:
		fmt.Println("Es mayor a 100")
	case value < 0:
		fmt.Println("Es menor a 0")
	default:
		fmt.Println("No condicion")
	}
}
```



## El uso de los keywords defer, break y continue

Defer:

```go
package main

import "fmt"

func main() {
	defer fmt.Println("Ejecutado al final pese a estar al comienzo gracias a 'Defer'.")
	fmt.Println("Hola Mundo")
}
```

Continue:

```go
package main

import "fmt"

func main() {
	var i uint8 = 0
	for i < 10 {
		fmt.Println(i)
		i++

		// Continue
		if i == 2 {
			fmt.Println("¡El número que sigue es par!")
			continue
		}

	}
}
```

Break:

```go
package main

import "fmt"

func main() {
	var i uint8 = 0
	for i < 10 {
		fmt.Println(i)
		i++

		// Break
		if i == 8 {
			fmt.Println("¡Break!")
			break
		}

	}
}
```

# 4. Estructuras de datos básicas

## Arrays y Slices

>  En Go, los arrays poseen un tamaño fijo y son inmutables, mientras que en los slices su tamaño es dinámico y los puedes modificar.

La diferencia principal entre los arrays es que estos tienen una longitud fija e invariable y deben declarase especifiandola

```go
x := [5]int{0, 1 ,2, 3, 4}
```

mientras que los Slices tienen una longitud variable y no hay que especificarla en la declaración

```go
var x [ ]float64
```

en este caso se crea un Slice con una longitud de cero
Si queremos crear un slice deberiamos usar la funcion make:

```go
x := make([]float64, 5)
```

esto crea un Slice asociado a un array subjacente de longitud 5.
Los Slices siempre están asociados a un array y aunque nunca pueden ser mas largos que el aray, pueden ser mas cortos.
La función make también permite un tercer parámetro, que representa la capacidad del array, por lo que

```go
x := make([]float64, 5, 10)
```

representa un Slice de longitud 5 y capacidad de 10

## Recorrido de Slices con Range

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	isPalindromo("Ama")
	isPalindromo("Amor a Roma")
}

func isPalindromo(text string) {
	var textReverse string

	for i := len(text) - 1; i >= 0; i-- {
		textReverse += string(text[i])
	}

	if strings.ToLower(text) == strings.ToLower(textReverse) {
		fmt.Println("Es palindromo")
	} else {
		fmt.Println("No es un palindromo")
	}
}
```

Solucion del reto

```go
func isPalindromo(text string) {
	var textReverse string
	text = strings.ToLower(text)// pasamos a minúsculas para que no genere error si se ingresan mayúsculas
	text = strings.ReplaceAll(text, " ", "") // quitamos espacios para usar palíndromos con espacios y no genere error
	for i := len(text) - 1; i >= 0; i-- {
		textReverse += string(text[i])
	}

	if text == textReverse {
		fmt.Println("Es Palindromo")
	} else {
		fmt.Println("No es un Palindromo")
	}
}

func main()  {	
	isPalindromo("Amita lava la tina") //No es un Palindromo
	isPalindromo("Anita Lava la tina") //Es Palindromo
}
```

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[strings · pkg.go.dev](https://godoc.org/strings)

## Llave valor con Maps

La estructura de datos MAP son más eficientes que los Array o Slices, ya que usan de forma nativa concurrencia para ejecutar las operaciones.

```go
package main

import "fmt"

func main() {
	m := make(map[string]int)

	m["Jose"] = 14
	m["Pepito"] = 20

	fmt.Println(m)


	// Recorrer un map
	for i, v := range m {
		fmt.Printf("%s tiene %d años\n", i, v)
	}

	// Encontrar un valor
	value, ok := m["Jose"]
	fmt.Println(value, ok)
}
```



![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[maps · pkg.go.dev](https://godoc.org/github.com/ross-oreto/go-list-map)

## Structs: La forma de hacer clases en Go

Sobre la definición, un *struct* es simplemente una **colección de campos.**

Los structs son la manera de definir una variable con diferentes tipos de datos. Parecen ser igual que en C.

Lo veria como una entidad, como el ejemplo clasico de la universidad, la clase persona que tiene la informacion completa de alguien como sus nombres, apellidos, edad, numero de identificacion y direccion de residencia, lo interesante es ver que podemos acceder a estos campos establecidos dentro del struct con el “.”, aun me resulta confuso un poco dimensionar un Struct como una clase de POO ya que, si bien en teoria puedo en ambas interactuar con metodos (que no se si hablas de las funciones o hay metodos tambien) y acceder a campos, hay algo en usabilidad que cambia, continuemos con el curso a ver que tal!

CODIGO:

```go
package main

import (
	"fmt"
)

type  car struct {
	brank string
	model string
	year int
	motor string
	cylinder string
	power string
	torque string
	gearbox string
	traction string
	color string
	seating int
}

func main() {
	myCar := car {
		brank: "Jeep",
		model: "Wrangler Unlimited Rubicon",
		year: 2020,
		motor: "V6 3.6L",
		cylinder: "3604 CC",
		power: "280 HP, 6400 RPM",
		torque: "35.4 Kgf.m @ 4100 rpm",
		gearbox: "Rock-Trac Active On Demand.",
		traction: "4x4",
		color: "Black",
		seating: 5,
	}
	fmt.Println("===============================")
	fmt.Println("brank =", myCar.brank)
	fmt.Println("model =", myCar.model)
	fmt.Println("year =", myCar.year)
	fmt.Println("moto =", myCar.motor)
	fmt.Println("cylinder = ", myCar.cylinder)
	fmt.Println("power =", myCar.power)
	fmt.Println("torque =", myCar.torque)
	fmt.Println("traction =", myCar.traction)
	fmt.Println("color = ", myCar.color)
	fmt.Println("seating = ", myCar.seating)
	fmt.Println("===============================")
}
```

RESULTADO:

```bash
===============================
brank = Jeep
model = Wrangler Unlimited Rubicon
year = 2020
moto = V6 3.6L
cylinder =  3604 CC
power = 280 HP, 6400 RPM
torque = 35.4 Kgf.m @ 4100 rpm
traction = 4x4
color =  Black
seating =  5
===============================
```

## Modificadores de acceso en funciones y Structs

Go modules nos puede ayudar a evitar un poco el $GOPATH, basta con ejecutar en caso en la carpeta “src”:

```bash
go mod init "elnombrequequieras"
```

Y poder usar lo que derive de el, mas información:

[go modules](https://golang.org/doc/tutorial/create-module)

Anteriormente en GO se tenía que trabajar el código dentro del GOPATH. Este GOPATH es una de las variables de entorno que declaramos al principio del curso (**export GOPATH=$HOME/go**) y no es más que la ruta a nuestro entorno de trabajo en donde encontraremos/crearemos la carpeta source que es donde trabajamos todo nuestro código.

Afortunadamente desde la **versión 1.11** podemos realizar nuestros proyectos de Go desde donde queramos gracias a los módulos.

Veamos un ejemplo.

Cree un proyecto llamando **0.0-fundamentals** en la siguiente ruta **/home/est14/my-own-path/15-go-course/0.0-fundamentals** como se puede apreciar estoy fuera de mi GOPATH (**/home/est14/go/source**).

Ahora cree mi **go.mod** aquí, en la raíz de **0.0-fundamentals** con el siguiente comando.

```bash
go mod init est14/0.0-fundamentals 
```

Esto me creo el archivo **go.mod** con el siguiente contenido :

```bash
module est14/0.0-fundamentals

go 1.16  # Esta es la version de Go
```

Luego cree un archivo llamado **main.go**, al mismo nivel de este cree un directorio llamado **greetings**, dentro de greetings cree un archivo llamado **greetings.go** y finalmente cree una función llamada **Hola**.

```bash
Mi estructura quedo asi:

├── go.mod
├── greetings
│ └── greetings.go // Aquí cree mi funcion Hola
└── main.go
```

Ahora mi intención era traer la función llamada **Hola** que estaba en **greetings.go** a mi archivo **main.go** y eso lo hice importando mi paquete “**est14/0.0-fundamentals/grettings**”

```go
//Este es el contenido de mi archivo main.go
package main

import (
	"est14/0.0-fundamentals/grettings"
	
)

func main() {

	greetings.Hola()
	
}
```

Y listo finalmente pude importar mi paquete y utilizar mi función Hola. Hay mucho detrás de todo esto y sería bueno detenerse un poco aquí ir por un helado luego leer un poco de documentación y seguir 😉.

Aqui como resolvi el tema del paquete:

![Screen Shot 2021-03-24 at 1.27.37 PM.png](https://static.platzi.com/media/user_upload/Screen%20Shot%202021-03-24%20at%201.27.37%20PM-0d897a64-8b9a-41cf-9c3a-3d43bc54be6c.jpg)

go run src/main.go

https://faun.pub/golang-package-management-using-go-modules-d3c929900114

# 5. Métodos e interfaces

## Structs y Punteros

[CS50 2020 - Lecture 4 - Memory](https://www.youtube.com/watch?v=NKTfNv2T0FE&t=494s)

Allí explican por qué lucen algo como `0xc0000b8010` spoiler está en hexadecimal el número y el `0x` es un prefijo en la computación cuando se escribe un número hexadecimal.

```go
package main

import "fmt"

type pc struct {
	ram int
	disk int
	brand string
}

// Creat funcion a una estructura
func (myPc pc) ping() {
	fmt.Println(myPc.brand, "Pong")
}

func (myPc *pc) duplicateRAM() {
	myPc.ram = myPc.ram * 2
}

func main() {
	a := 50
	b := &a

	fmt.Println(a, &a, b, *b)

	*b = 100
	fmt.Println(a)


	myPc := pc{ram: 16, disk: 200, brand: "msi"}
	fmt.Println(myPc)
	myPc.ping()

	myPc.duplicateRAM()
	fmt.Println(myPc)
	
	myPc.duplicateRAM()
	fmt.Println(myPc)
	
}
```

- Podemos asociar una función o método a un struct.
- Si no indicamos el apuntador, es posible acceder a los valores de las variables, pero no es posible modificarlos desde el método.
- Si pasamos el apuntador al método, entonces tendremos acceso a la memoria donde podremos modificar los valores.

Para solucionar el reto, creé una carpeta llamada “pcpackage” y dentro, un archivo llamado “pcpackage.go”. Allí, escribí el siguiente código:

```go
package pcpackage

import "fmt"

// PC struct with public access
type PC struct {
	Ram   int
	Disk  int
	Brand string
}

func (myComputer *PC) DuplicateRAM() {
	myComputer.Ram = myComputer.Ram * 2
	fmt.Println(myComputer)
}
```

Luego, en el main.go para acceder al struct creado en pcpackage, escribí el siguiente código:

```go
package main

import (
	computer "curso_golang_platzi/src/pcpackage"
	"fmt"
)

func main() {
	var newComputer computer.PC
	newComputer.Ram = 32
	newComputer.Disk = 256
	newComputer.Brand = "Razer"

	fmt.Println(newComputer)
	newComputer.DuplicateRAM()

	// Output:
	// {32 256 Razer}
	// &{64 256 Razer}
}
}```

Espero que sea de utilidad :)
```

## Stringers: personalizar el output de Structs

- La estructura de datos " Struct " tiene un método llamado " String " , que podemos sobrescribir para personalizar la salida a consola de los datos del struct.
- Creo que los stringers es muy parecido a lo que Python tiene para las clases que se llama el método `__src__`

Por lo que puedo ver, lo que va entre func y el nombre de la funcion (miPC pc) es lo que asocia a la funcion con la struct, pero no necesariamente con el “objeto” miPC, ahi puede ir cualquier nombre. Ej:

```go
// Stringer
func (juanito pc) String() string {
	return fmt.Sprintf("PC: %s con %dGB RAM y SSD %dGB", juanito.brand, juanito.ram, juanito.disk)
}
```

y por otro lado, note algo que esta bueno me parece. Como el struct se inicializa con pares clave:valor entre llaves {}, no hace falta que esten en orden, se lo cambie y funciona igual:

```go
type pc struct {
	ram   int
	disk  int
	brand string
}
...
	// con la struct
	miPC := pc{ram: 16, brand: "Dell", disk: 480}
	fmt.Println(miPC) // llama a String
```

Los stringers nos sirven para modificar la forma en la que se imprime un struct, se hacen de la siguiente manera:

```go
package main

type pc struct {
	ram int
	model string
}

func (myPc pc) String() string {
	return fmt.Sprintf("Es un %s model, y tiene %d RAM")
}

func main() {
	myPc := pc{model: "Asus", ram: 16}
	fmt.Println(myPc)
}
```

El output de el Println sera de esta forma:

```shell
$ go run main.go
Es un Asus model, y tiene 16 RAM
```

Si no hubieramos creado el stringer el output seria:

```shell
$ go run main.go
{16 500 Msi}
```

## Interfaces y listas de interfaces

- Si los structs que tenemos en el código tienen métodos que hacen algo en común (Cálculos, obtener data, etc), es posible ejecutar éstos métodos usando una interfaz, de esta forma evitamos hacer código por cada struct.

Reto:
**main.go**

```go
package main

import (
	fg "curso-golang/src/18.Interfaces/figuras"
	"fmt"
)

type figuras2D interface {
	Area() float64
}

func calcular(f figuras2D) {
	fmt.Printf("Area: %v\n", f.Area())
}

func main() {
	miCuadrado := fg.NewCuadrado(2)
	miRectangulo := fg.NewRectangulo(2, 4)
	miCirculo := fg.NewCirculo(3.2)
	miTriangulo := fg.NewTriangulo(7.8, 2.9)

	calcular(miCuadrado)
	calcular(miRectangulo)
	calcular(miCirculo)
	calcular(miTriangulo)
}
```

**cuadrado.go**

```go
package figuras

type cuadrado struct {
	base float64
}

func NewCuadrado(bs float64) cuadrado {
	return cuadrado{base: bs}
}

func (c cuadrado) Area() float64 {
	return c.base * c.base
}
```

**rectangulo.go**

```go
package figuras

type rectangulo struct {
	base   float64
	altura float64
}

func NewRectangulo(bs, alt float64) rectangulo {
	return rectangulo{base: bs, altura: alt}
}

func (r rectangulo) Area() float64 {
	return r.altura * r.base
}
```

**circulo.go**

```go
package figuras

import "math"

type circulo struct {
	radio float64
}

func NewCirculo(rd float64) circulo {
	return circulo{radio: rd}
}

func (c circulo) Area() float64 {
	return math.Pi * c.radio * c.radio
}
```

**triangulo.go**

```go
package figuras

type triangulo struct {
	base   float64
	altura float64
}

func NewTriangulo(bs, alt float64) triangulo {
	return triangulo{base: bs, altura: alt}
}

func (t triangulo) Area() float64 {
	return t.base * t.altura / 2
}
```

# 6. Concurrencia y Channels

## ¿Qué es la concurrencia?

Una manera interesante de ver alguno de los problemas con la concurrencia es con el uso de maps.
por ejemplo si ejecutamos el siguiente código:

```go
package main

import "fmt"

func main() {
	m := make(map[string]int)

	m["1"] = 1
	m["2"] = 2
	m["3"] = 3
	m["4"] = 4
	m["5"] = 5
	m["6"] = 6
	m["7"] = 7
	m["8"] = 8
	m["9"] = 9
	m["10"] = 10

	for _, value := range m {
		fmt.Printf("%d\n", value)
	}
}
```

Van a haber momentos donde no se va a imprimir en orden correcto.

Por lo que entendi de la definicion es que concurrencia puede manejar varias peticiones en un solo input por ejemplo, mientras, paralelismo hace varias cosas en una sola instancia

La concurrencia está alineando con múltiples cosas al mismo tiempo mientras que el paralelismo está haciendo múltiples cosas al mismo tiempo.

```go
Concurrencia != Paralelismo
```

Les comparto un video donde explican muy bien la diferencia entre estos [2 terminos](https://www.youtube.com/watch?v=mRn0exsKRCA) 😃

## Primer contacto con las Goroutines

Un **WaitGroup** espera a que una colección de goroutines termine su ejecución.
Para esto se una la **WaitGroup.Add()** ( wg.add(1) en el ejemplo de la clase).
El número entero indica el número de goroutines que debe esperar para finalizar la ejecución de la goroutine principal.

Cada vez que una goroutine termina su ejecución, llama el método Done(). Esto hace que el contador del WaitGroup se reduzca.
Cuando el contador llegue a zero la rutina principal continuará su ejecución.

La función **wait()** bloquea la rutina principal hasta que todas las demás rutinas del grupo hayan terminado.

```go
package main

import (
	"fmt"
	"sync"
	"time"
)


func say(text string, wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Println(text)
}

func main() {

	var wg sync.WaitGroup

	fmt.Println("Hello")

	wg.Add(2)
	go say("world", &wg)
	go say("!", &wg)
	
	wg.Wait()
	
	go func(text string) {
		fmt.Println(text)
	}("Adios")

	time.Sleep(time.Second * 1)
}

```

**Example**

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

func say(text string, wg *sync.WaitGroup) { // Gorutine

	defer wg.Done() // Esta linea se va a ejecutar hasta el final de la funcion, y de esta forma libera el gorutine del WaitGroup

	fmt.Println(text)
}

func main22() {

	var wg sync.WaitGroup // El paquete sync permite interacturar de forma primitiva con las gorutine. Variable que acomula un conjunto de gorutines y los va liberando poco a poco

	fmt.Println("Hello")

	wg.Add(1) // Indicamos que vamos a agregar 1 Gorutine al WaitGroup para que espere su ejecucion antes de que la gurutine base (main) muera, y así le de tiempo a la siguiente gorutine de ejecutarse

	go say("world", &wg) // la palabra reservada go ejecutará la funcion de forma concurrente

	wg.Wait() // Funcion del WaitGroup que sirve para decirle al gorutine principal (main) que espere hasta que todas las gorutine del WaitGroup finalicen, es decir, hasta que se ejecute 'defer wg.Done()' en cada una de las goroutines

	go func(text string) { // Funciona anonima
		fmt.Println(text)
	}("Adios")

	time.Sleep(time.Second * 1) // ! Funcion para que cuando llegue a esta linea espere el tiempo indicado (lo suficiente para que la Gorutine ejecute su funcion de forma concurrente)

	// Nota: Para fines practicos se hace uso de la funcion Sleep(), pero en realidad NO es una buena practica, es mejor utilizar los WaitGroups

}
```

## Channels: La forma de organizar las goroutines

- Goroutines sería útil a la hora de procesar documentos o imágenes.

ejemplo para imprimir la salida del Channel:

```GO
func printTextOurCh(c <-chan string) {
	fmt.Println(<-c)
}
```

**Example**

```go
package main

import "fmt"

func say(text string, c chan<- string) {
	c <- text
}

func main() {
	c := make(chan string, 1)

	fmt.Println("Hello")

	go say("Bay", c)

	fmt.Println(<-c)
}
```

## Range, Close y Select en channels

```go
package main

import "fmt"

func message(text string, c chan string) {
	c <- text
}

func main() {
	c := make(chan string, 2)

	c <- "Mensaje 1"
	c <- "Mensaje 2"

	fmt.Println(len(c), cap(c))

	// Range y close
	close(c)
	//c<-"Mensaje 3"

	for message := range c {
		fmt.Println(message)
	}

	// Select
	email1 := make(chan string)
	email2 := make(chan string)

	go message("mensaje 1", email1)
	go message("mensaje 2", email2)

	for i := 0; i < 2; i++ {
		select {
		case m1 := <-email1:
			fmt.Println("Email recibido de email 1", m1)
		case m2 := <-email2:
			fmt.Println("Email recibido de email 2", m2)
		}
	}
}

```

# 7. Manejo de paquetes y Go Modules

## Go get: El manejador de paquetes

Tips para los que estan batallando:

1. Asegurence que estan en la carpeta de GOPATH
2. Creen un archivo del proyecto y hagan un mod init (nombre de carpeta)
3. Go get del repo
4. go mod tidy para obtener lo faltante (ayuda mucho para paquetes locales)

Paquetes locales extra:

- go mod edit -replace=(nombre del paquete)=(direccion local del paquete)
  Ejemplo:
- go mod edit -replace=exampl e. com/greetings=…/greetings

Para que funcione tour reemplazar:

```bash
go get golang.org/x/tour
```

Por:

```bash
go get golang.org/x/website/tour 
```

Pagkage “ECHO”:

1. Instalacion:

```bash
go get -u github.com/labstack/echo/... 
```

1. Archivo “echoServer.go”:

```go
package main

import (
	"net/http"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Route => handler
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!\n")
	})

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
} 
```

Un framework que me encanta para desarrollar APIs es [Fiber](https://gofiber.io/), te da un boost de performance a tu proyecto ya que usa `fasthttp` en lugar del por defecto `net/http` que ya tiene **GO**

Para instalarlo solo ocupas escribir en tu terminal:

```bash
go get github.com/gofiber/fiber/v2
```

Y el hello world es:

```go
package main

import "github.com/gofiber/fiber/v2"

func main() {
  app := fiber.New()

  app.Get("/", func(c *fiber.Ctx) error {
    return c.SendString("Hello, World!")
  })

  app.Listen(":3000")
}
```

Si vienes del mundo de node.js, usando `Express` o `Fastify` pueden observar la sintaxis es muy similar pero mejor, por que estamos usango GO 

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[A curated list of awesome Go frameworks, libraries and software - Awesome Go](http://awesome-go.com/)

## Go modules: Ir más allá del GoPath con Echo

Cuando usas god mods no es necesario estar en el GOPATH, god mods nos permite inicializar y/o trabajar nuestro proyecto en cualquier parte de nuestro equipo, no importa si estas en
Documents o en algun otro sitio, esta caracteristica fue pedida por la comunidad dado que algunos no se sentian tan comodos al estar obligados a trabajar en el GOPATH

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[Modules · golang/go Wiki · GitHub](https://github.com/golang/go/wiki/Modules)

## Modificando módulos con Go

```go
• go mod edit -replace=exampl e. com/greetings=…/greetings

Go mod verify

Go mod edit -dropreplace …
Go mod vendor
Go sum: Repertorio de todas las librerias (verificar)
Go mod tidy: Limpiar librerias no usadas

```

Si quiren saber más sobre como mnejar los modulos en go. Les dejo este link de la documentación oficial.
https://blog.golang.org/using-go-modules

Dejó también un pequeño resumen de los comandos que se explican.

```bash
go mod init
```

- Crea un nuevo módulo, inicializando el archivo go.mod que lo describe.

```bash
go build, go test, and other package-building commands
```

- Agrega nuevas dependencias a go.mod según sea necesario.

```bash
go list -m all
```

- Imprime las dependencias del módulo actual.

```bash
go get
```

- Cambia la versión requerida de una dependencia (o agrega una nueva dependencia).

```bash
go mod tidy
```

Elimina las dependencias no utilizadas.

![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)[Modules · golang/go Wiki · GitHub](https://github.com/golang/go/wiki/Modules)

# 8. Despedida del curso

## Despedida



![img](https://www.google.com/s2/favicons?domain=http://tour.golang.com//favicon.ico)[A Tour of Go](http://tour.golang.com/)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[play-with-go.dev](https://play-with-go.dev/)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Go by Example](https://gobyexample.com/)

![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)[Slack](http://gophers.slack.com/)

![img](https://www.google.com/s2/favicons?domain=https://open.scdn.co/cdn/images/favicon32.a19b4f5b.png)[Spotify](https://open.spotify.com/show/2cKdcxETn7jDp7uJCwqmSE?si=q88UkEYQTxS0t1QVws22tw&amp;nd=1)

# 9. Bonus

## Cheat Sheet Go

En esta lectura te llevarás algunos tópicos que siempre vale la pena tener a la mano al momento de programar en Go, además te dejo algunos tips extras.

- Hola mundo

```Go
package main

import "fmt"

func main(){
    fmt.Println("Hola mundo")
}
```

- ¿Hacer una impresión en consola rápida?

```Go
package main

func main(){
    print("Hola")
}
```

- Importar una librería sin usarla

```Go
package main

/*
    Hazlo solo y únicamente cuando la librería externa
    que estés usando lo pida explícitamente
*/
import ( 
    "fmt"
    _ "math"
)

func main(){
    fmt.Println("Hola mundo")
}
```

- Agregar un alias a un import (no suele usarse, pero es bueno saberlo)

```Go
package main

import (
	"fmt"
	mth "math"
)

func main() {
	fmt.Println(mth.Pi)
}
```

- Diferentes formas de declarar variables

```Go
v := 12
var v int = 12
var v int
```

- Zero values de primitivos

```Go
var a int // 0
var b float64 // 0
var c string // ""
var d bool // false
```

- Incremental y decremental

```Go
x++ // Suma 1 a x
x-- // Resta 1 a x
```

- Imprimir tipo de variables (hay otras formas, pero esta es la más fácil)

```Go
a := 2
fmt.Printf("%T", a)
```

- Función para tomar los errores (ahorra mucho código)

```Go
func isError(e error) {
    if e != nil {
        log.Fatal(e)
    }
}

// Ejemplo de uso
func main() {
	_, err := strconv.Atoi("53a")
	isError(err)
}
```

- Arrays vs Slices

```Go
// Array
var myList [2]int

// Slice
var myList2 []int
```

- Slice de interfaces (Úsalo con sabiduría)

```Go
// Permite guardar diferentes tipos de datos en un mismo slice
myList := []interface{}{"Hola", 12, 4.90}

// Iterar sobre los distintos tipos de datos de ese slice
for _, v := range myList {
    switch v.(type) {
    case int:
        fmt.Println("Es int")
    case string:
        fmt.Println("Es string")
    case float64:
        fmt.Println("Es float64")
    }
}
```

- Asegurarnos si un key existe en el map

```Go
m := make(map[string]int)

m["hola"] = 1

// Nota, usalmente se usa "ok" para recibir la segunda variable
value, ok := m["hello"]

/*
Si existe, ok será "true"
Si no existe, ok será "false"

En este caso, ok es "false" porque no existe.
*/
```

- Punteros

```Go
a := 10 // Variable int
b := &a // "b" es el puntero de "a"
c := *b // "c" adquiere el valor del puntero de "b", es decir toma el mismo valor de "a"
```

- Comandos de Go modules

```Go
// Inicializar un proyecto
go mod init path_del_proyecto

// Verificar que el código externo no esté corrupto
go mod verify

// Reemplazar fuente del código
go mod edit -replace path_del_repo_online=path_del_repo_en_local

// Quitar el replace
go mod edit -dropreplace path_del_repo_online

// Empaquetar todo el código de terceros que usa nuestro código
go mod vendor

// Eliminar todos los paquetes externos que no estemos usando
go mod tidy

// Aprender más de go modules
go help mod
```

- Nota personal

> Aún tienes un largo camino por recorrer. Pero lo que más quiero que te lleves de este curso son tres cosas: Practica, estudia y participa en la comunidad de Go.
>
> [Cheat Sheet Golang](https://devhints.io/go)

## Data Science con Go

Go, así como lo viste en el curso tiene características muy particulares que lo hace encajar muy bien en el mundo del Data Science. Especialmente en estas tres características:

- Manejar tareas pesadas con múltiples hilos de ejecución a través de la concurrencia es muy sencillo.
- Es compilado, lo que lo hace mucho más eficiente.
- Optimiza muy bien los recursos del hardware.

Por tal motivo, esta lectura está enfocada en mostrar el estado del arte actual de Go en Data Science.

## Jupyter

<img src="https://i.ibb.co/rMk9sbt/labpreview.png" alt="labpreview" border="0">

[Jupyter](https://jupyter.org/) es una de las principales herramientas que utilizamos los Data Scientists en el día a día ya que nos permite ejecutar código de manera fácil e iterativa pudiendo reciclar variabless en cualquier momento.

Al momento de instalarlo vía `pip` (manejador de paquetes para Python) viene instalado con el kernel de Python listo para ejecutar código Python.

A pesar, que Go es un lenguaje compilado, la comunidad ha creado un intérprete de Go llamado [gomacro](https://github.com/cosmos72/gomacro) para ejecutar código sin compilar. Partiendo de ello, crearon también un kernel de Go para usarlo en Jupyter Notebook llamado [gophernotes](https://github.com/gopherdata/gophernotes)

<img src="https://i.ibb.co/FDsPQJP/jupyter.gif" alt="jupyter" border="0">

De los métodos de instalación, mi sugerencia es que utilices la vía de Docker.

## Manejo de DataFrames

En este punto los más populares son: [qframe](https://github.com/tobgu/qframe), [gota](https://github.com/go-gota/gota) y [dataframe-go](https://github.com/rocketlaunchr/dataframe-go).

Hasta la fecha, ninguno está en su versión estable, pero todos están haciendo un gran trabajo porque como lo notaste en el curso, en Go las variables vacías no son nulas sino que tienen un valor por defecto. Y esto en el mundo de los datos es todo un reto ya que tener datos nulos es el pan de cada día.

## Visualizaciones

En este apartado destacan dos:

- [gonum/plot](https://github.com/gonum/plot): Gonum no solo es el [Numpy](https://numpy.org/) en Go sino que además tiene su propio código de visualización. En este caso gonum/plot te permite hacer visualizaciones estáticas. Te comparto algunos ejemplos:

<img src="https://i.ibb.co/gT07CCT/points-commas.png" alt="points-commas" border="0">

<img src="https://i.ibb.co/0YmCv7Q/barchart3.png" alt="barchart3" border="0">

<img src="https://i.ibb.co/WxyDPnH/boxplot.png" alt="boxplot" border="0">

- [go-echarts](https://github.com/go-echarts/go-echarts): Para el caso de gráficos interactivos esta es una de las mejores opciones ya que además de los gráficos puedes crear tu propio dashboard con la misma librería.

<img src="https://i.ibb.co/mJN2qFf/go3.gif" alt="go3" border="0">

<img src="https://i.ibb.co/PFxn4VT/go2.gif" alt="go2" border="0">

<img src="https://i.ibb.co/PFxn4VT/go2.gif" alt="go2" border="0">

## Machine Learning

Hay muchas librerías este ámbito, sin embargo, destacaré solo dos:

- [GoLearn](https://github.com/sjwhitworth/golearn): Tiene diferentes modelos, entre ellos lineales, regresiones y clasificación.
- [Gorgonia](https://github.com/gorgonia/gorgonia): Es el más popular cuando requerimos implementar [Deep Learning](https://platzi.com/cursos/ia/) sino que además tiene la opción de implementar [CUDA](https://developer.nvidia.com/CUDA-zone) (hacer modelos de Deep Learning usando la tarjeta gráfica Nvidia)

------

La gran mayoría de estas librerías apenas están en sus inicios, pero tienen lo mínimo para iniciar en Data Science aplicando Go. Mi manera de apoyar a la comunidad es creando una librería para manejo de dataframes con Go llamada [Higor](https://github.com/osmandi/higor) usando mi experiencia como Data Engineer y haciendo que su implementación sea lo más amigable posible.


## Librerías para desarrollo web con Go

# Librerías para desarrollo Web con Go

Actualmente puedes usar Go para crear tu aplicación web sin necesidad de implementar librerías externas como sucede en muchos otros lenguajes. Aunque claro, esto tiene sus pros y sus contras. Pero al final del día todo dependerá del flujo de trabajo de tu equipo.

Sin embargo, te quiero compartir las librerías y frameworks de desarrllo web que se encuentran entre las más populares.

## FrontEnd - Hugo

![hugo_logo](https://d33wubrfki0l68.cloudfront.net/c38c7334cc3f23585738e40334284fddcaf03d5e/2e17c/images/hugo-logo-wide.svg)

[Hugo](https://gohugo.io/) es el framework más popular para usar Go en el FrontEnd, claro no es que se utilice en FrontEnd específicamente sino que tiene su propio template para generar archivos estáticos (sin BackEnd, solo HTML, CSS y JavaScript).

Pero la mayor ventaja de Hugo, es que tienen [Hugo Themes](https://themes.gohugo.io/) una tienda de templates donde muchos miembros de la comunidad suben sus temas para disponerlos al resto de la comunidad para su uso, solo debes prestar atención a la licencia. Pero encontrarás temas con todo tipo de features, desde uso para portafolio, compatibilidad con [Google Anlytics](https://platzi.com/cursos/google-analytics/) y mucho más.

Y si lo combinas con [Vercel](https://platzi.com/clases/2011-despliegue-apps/31609-despliegue-en-vercel/) podrás desplegar tu aplicación de forma gratuita.

## BackEnd

Para el caso de BackEnd tenemos muchas opciones, las que te recomiendo probar son las siguientes:

### Echo

![echo_image](https://echo.labstack.com/images/terminal.png)

[Echo](https://echo.labstack.com/) es el Framework que más he llegado a usar en las Apps en Go que he realizado. Esto porque reúne varias características:

- Es minimalista, evitando tener código que no vamos a usar
- Fácil de escalar y un excelente rendimiento al momento de compilar
- Tiene su sistema de enrutamiento, pudiendo incluso implementar middlewares (proceso de autenticación)
- Tiene su propia interfaz para implementar websockets (conecciones en tiempo real)
- Puedes usarlo tanto para crear APIs como páginas web completas.
- Es uno de los pocos framewords que tiene su propia implementación de JWT (otro tipo de autenticación muy usado en APIs)

### Gin-Gonic

![img](https://avatars3.githubusercontent.com/u/7894478?s=200&v=4)

[Gin-Gonic](https://gin-gonic.com/) Es un framework al que fácilmente le puedes agarrar cariño y esto es porque la forma como interactúas para crear Apps es muy intuitiva. De momento no es de los Frameworks que más destaque pero tampoco se queda atrás. Ideal para tus primeros proyectos de BackEnd con Go.

### Beego

![beego_logo](https://beego.me/static/img/beego_purple.png)

[Beego](https://beego.me/) por bastante tiempo llegó a ser uno de los Frameworks más usados, no es que haya dejado de serlo, pero trajo a la comunidad 3 grandes features:

- Su propio ORM (Object Relational Mapping), básicamente son un conjunto de métodos para interactuar con la base de datos desde consulta a creación de tablas.
- Un CLI (Command Line Interface) llamado [bee](https://github.com/beego/bee) con el cual puedes generar proyecto ya sea para hacer una API como página web completa, empaquetar el proyecto listo para llevarlo a producción y hacer migraciones de bases de datos.
- Un admin, ¡sí un admin! Con el cual puedes ver estadísticas de las rutas de tu app, verificar el estado de la base de datos y ejecutar tareas de forma manual

Hoy en día Beego tiene muchos más features como por ejemplo crear el Dockerfile para que tu app se ejecute en [Docker](https://platzi.com/cursos/docker/). Si vas a crear múltiples apps seguidamente, te recomiendo este Framework.

### Revel

![revel_logo](https://revel.github.io/img/revelhat.png)

[Revel](https://revel.github.io/) es uno de los pesos pesados en desarrollo web para Go incluso fue conocido como el [django](https://platzi.com/cursos/django/) de Go (django es el framework web más popular para Python) pero dejó de recibir actualizaciones por un tiempo significativo y también perdió un poco esa posición.

Hoy en día, Revel no solo ha llegado con nuevo look sino que también le han invertido a tener una de la documentaciones más completas incluso con apps de ejemplo.

### Gorilla

![gorilla_logo](https://camo.githubusercontent.com/a62a5e2040257dd8787001ffa5d95964d7bc77024aa2ba3d94e64ec1e151228e/68747470733a2f2f636c6f75642d63646e2e7175657374696f6e61626c652e73657276696365732f676f72696c6c612d69636f6e2d36342e706e67)

[Gorilla](https://www.gorillatoolkit.org/) no es un Framework, ellos se definen como con herramientas para web. No tiene un ORM, tampoco un CLI, tampoco render de HTML, tampoco un admin. Entonces ¿por qué está en esta lista?

Pues en `gorilla` se enfocaron en una cosa hasta hacerla muy bien, estos son los WebSockets (para mantener conexiones en tiempo real por ejemplo un chat, algo en lo que Go destaca y por mucho). Con el paso del tiempo han agregado más features, como por ejemplo manejos de cookies, enrutador, etc.

Puedes desarrollar una web completa usando las librerías nativas de Go y con `gorilla` tienes esos extras que te ahorran implementación. Han hecho tan bien el código que incluso framworks web lo usan en su código sobre todo la parte de websockets.

### Buffalo

![buffalo_logo](https://gobuffalo.io/assets/images/logo_med.png)

[Buffalo](https://gobuffalo.io/en/) es otro de los pesos pesados en frameworks para web con Go. Hoy en día, es uno de los más completos tiene CLI, ORM, manejo de cookies, middlewares, templates para reder de HTML, empaquetador con Docker e incluso puedes hacer despliegue a [Heroku](https://platzi.com/clases/2011-despliegue-apps/31612-que-es-heroku/), [Digital Ocean](https://platzi.com/cursos/digital-ocean/) y [Azure](https://platzi.com/azure/) con el mismo CLI de Buffalo. Junto a otros features adicionales.

Sin embargo, el que sea más completo no significa que sea fácil de implementarlo. Es fácilmente recomendable para aquellas aplicaciones que necesite ser robustas desde el principio, mi sugerencia acá es que no seas el único(ca) desarrollador(ra) en el equipo ya que el mantenimiento es tan pesado como lo es la librería.

## Extra

Como programdor(ra) en más de una ocasión debes crear documentación de la API que haz desarrollado, pues no suele ser de las tareas más divertidas al desarrollar. Pero si te dijera que puedes crear documetación en la medida que desarrollas ¿me creerías?

Digamos que sí me crees, ¿me seguirías creyendo si te dijera que generaría toda un interfaz parecida a esta?

![img](https://static1.smartbear.co/swagger/media/images/tools/opensource/swagger_ui.png?ext=.png)

Esto se pone más intersante, ahora imagina que esa documetación autogenerada además maneja autenticación e incluso es interactiva. Puedes verlo por ti mismo(ma) en este [swagger de ejemplo](https://petstore.swagger.io/).

Allí es donde entra [swaggo](https://github.com/swaggo/swag)

![swaggo_logo](https://raw.githubusercontent.com/swaggo/swag/master/assets/swaggo.png)

[swaggo](https://github.com/swaggo/swag) es la forma como podemos implementar swagger en nuestro código en Go. De los frameworks que te he compartido acá, es compatible con Gin-Gonic, echo y búffalo. Beego, por su parte, está incluido en su CLI.

Como punto adicional, dependiendo de la librería los comentarios para hacer la documentación en `swagger` pueden generarse de forma automática o bien requiere una cierta intervención manual.